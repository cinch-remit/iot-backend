import {SET_DASHBOARDS, UPDATE_DASHBOARD} from "../../actions/dashboard/dashboardActions";

const defaultState = {
    dashboards: []
}

const dashboards = (state = defaultState, action) => {
    switch (action.type) {
        case SET_DASHBOARDS:
            return {
                ...state,
                dashboards: action.payload
            }

        case UPDATE_DASHBOARD:
            const data = action.payload;
            const dashboardsWithoutDashboardToBeUpdated = [...state.dashboards].filter(dashboard => dashboard.name !== data.name);
            return {
                ...state,
                dashboards: [...dashboardsWithoutDashboardToBeUpdated, data]
            }

        default:
            return state;
    }
}

export default dashboards;