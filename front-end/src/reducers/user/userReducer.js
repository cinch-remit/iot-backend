import {LOAD_NOTIFICATIONS, LOGIN, LOGOUT, STATUS, UPDATE_NOTIFICATION} from "../../actions/user/userActions";

const defaultState = {
    user: {},
    status: '',
    notifications: [],
    loggedIn: false
}


const user = (state = defaultState, action) => {
    switch (action.type) {
        case LOGIN:
            return {
                ...state,
                user: action.payload,
                loggedIn: true
            }
        case LOGOUT:
            return {
                ...state,
                user: {},
                loggedIn: false
            }
        case STATUS:
            return {
                ...state,
                status: action.payload
            }
        case LOAD_NOTIFICATIONS: {
            return {
                ...state,
                notifications: action.payload
            }
        }
        case UPDATE_NOTIFICATION: {
            return {
                ...state,
                notifications: [action.payload, ...state.notifications]
            }
        }
        default:
            return state;
    }
}

export default user;