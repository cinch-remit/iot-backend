import {SET_BUILD_INFO, WELCOME_MESSAGE} from "../../actions/info/appinfoActions";

const defaultState = {
    buildInfo: {},
    welcomeMessage: false
}

const appinfo = (state=defaultState, action) => {
    switch (action.type) {
        case SET_BUILD_INFO:
            return {
                ...state,
                buildInfo: action.payload
            }

        case WELCOME_MESSAGE:
            return {
                ...state,
                welcomeMessage: action.payload
            }
        default:
            return state
    }
}

export default appinfo;