import {combineReducers} from "redux";
import user from "./user/userReducer";
import devices from "./device/deviceReducer";
import appinfo from "./info/appinfoReducer";
import dashboards from "./dashboard/dashboardReducer";

const rootReducer = combineReducers({
    user: user,
    devices: devices,
    appInfo: appinfo,
    dashboard: dashboards
});

export default rootReducer;