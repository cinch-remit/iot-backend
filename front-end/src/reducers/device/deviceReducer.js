import {SET_DEVICES, UPDATE_DEVICE} from "../../actions/device/deviceActions";

const defaultState = {
    devices: []
}

const devices = (state = defaultState, action) => {
    switch (action.type) {
        case SET_DEVICES:
            return {
                ...state,
                devices: action.payload
            }

        case UPDATE_DEVICE:
            const data = action.payload;
            const devicesWithoutDeviceToBeUpdated = [...state.devices].filter(device => device.deviceID !== data.deviceID)
            return {
                ...state,
                devices: [...devicesWithoutDeviceToBeUpdated, data]
            }

        default:
            return state
    }
}

export default devices;