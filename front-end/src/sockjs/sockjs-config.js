import * as SOCKJS from 'sockjs-client';
import * as StompJs from "@stomp/stompjs";

export const mySocketFactory = () => {
    return new SOCKJS('/notifications');
};

export const client = new StompJs.Client({
    webSocketFactory: function() {
        return new SOCKJS('/notifications')
    },
    // debug: function (str) {
    //     console.log(str);
    // },
    reconnectDelay: 5000,
    heartbeatIncoming: 4000,
    heartbeatOutgoing: 4000,
});

