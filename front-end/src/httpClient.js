export const doRequest = async (url, settings = null) => {
    return (fetch(url, settings));
}

export const defaultHeaders = {
    "Content-Type": "application/json",
}
