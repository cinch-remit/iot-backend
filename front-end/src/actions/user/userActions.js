import {defaultHeaders, doRequest} from "../../httpClient";

export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const STATUS = 'STATUS';
export const LOADING = 'LOADING';
export const FAILED = 'FAILED';
export const SUCCESS = 'SUCCESS';

const setUser = payload => ({
    type: LOGIN,
    payload
});

const setStatus = payload => ({
    type: STATUS,
    payload
});

export const doSetUser = () => dispatch => {
    dispatch(setStatus(LOADING))
    const settings = {
        method: "GET",
        headers: defaultHeaders
    }

    doRequest(`/v1/user/details`, settings)
        .then(res => {
            if (res.ok) {
                return res.json()
            }
            throw res;
        })
        .then(data => {
            dispatch(setUser(data));
        })
        .then(() => dispatch(setStatus(SUCCESS)))
        .catch(err => {
            console.log(err);
            dispatch(setStatus(FAILED));
        })
}

// notification
export const UPDATE_NOTIFICATION = "UPDATE_NOTIFICATION";
export const LOAD_NOTIFICATIONS = "LOAD_NOTIFICATIONS";
const updateNotification = payload => ({
    type: UPDATE_NOTIFICATION,
    payload
});

const loadNotifications = payload => ({
    type: LOAD_NOTIFICATIONS,
    payload
})

export const doUpdateNotification = payload => dispatch => {
    dispatch(updateNotification(payload));
}

export const doLoadNotifications = () => dispatch => {
    const settings = {
        method: "GET",
        headers: defaultHeaders
    }

    doRequest(`/v1/notifications`, settings)
        .then((res) => {
            if (res.ok) {
                return res.json();
            }
            throw res;
        })
        .then((res) => {
            dispatch(loadNotifications(res))
        })
        .catch(() => console.error("could not load notifications."));
}
