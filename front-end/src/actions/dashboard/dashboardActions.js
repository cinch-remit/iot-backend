import {defaultHeaders, doRequest} from "../../httpClient";

export const SET_DASHBOARDS = 'SET_DASHBOARDS';
export const UPDATE_DASHBOARD = 'UPDATE_DASHBOARD';


const setDashboards = payload => ({
    type: SET_DASHBOARDS,
    payload
});

const updateDashboard = payload => ({
    type: UPDATE_DASHBOARD,
    payload
});

export const doSetDashboards = dashboards => dispatch => {
    dispatch(setDashboards(dashboards));
}

export const loadDashboards = () => dispatch => {
    const settings = {
        method: 'GET',
        headers: defaultHeaders
    }

    doRequest(`/v1/dashboard`, settings)
        .then(res => {
            if (res.ok) {
                return res.json();
            }

            throw res;
        })
        .then(data => dispatch(setDashboards(data)))
        .catch(err => console.log(err));
}

export const loadDashboard = dashboardName => dispatch => {
    const settings = {
        method: 'GET',
        headers: defaultHeaders
    }

    doRequest(`/v1/dashboard/${dashboardName}`, settings)
        .then(res => {
            if (res.ok) {
                return res.json();
            }
            throw res;
        })
        .then(data => dispatch(updateDashboard(data)))
        .catch(err => {
            console.log(err);
        });
}