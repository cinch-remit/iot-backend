import {defaultHeaders, doRequest} from "../../httpClient";

export const SET_DEVICES = 'SET_DEVICES';
export const UPDATE_DEVICE = 'UPDATE_DEVICE'

const setDevices = payload => ({
    type: SET_DEVICES,
    payload
});

const updateDevice = payload => ({
    type: UPDATE_DEVICE,
    payload
});

export const doSetDevices = devices => dispatch => {
    dispatch(setDevices(devices))
};

export const loadDevices = () => dispatch => {
    const settings = {
        method: 'GET',
        headers: defaultHeaders
    }

    doRequest(`/v1/device/all`, settings)
        .then(res => {
            if (res.ok) {
                return res.json();
            }
            throw res;
        })
        .then(data => dispatch(setDevices(data)))
        .catch(err => {
            console.log(err);
        });
}

export const loadDevice = deviceId => dispatch => {
    const settings = {
        method: 'GET',
        headers: defaultHeaders
    }

    doRequest(`/v1/device/${deviceId}`, settings)
        .then(res => {
            if (res.ok) {
                return res.json();
            }
            throw res;
        })
        .then(data => dispatch(updateDevice(data)))
        .catch(err => {
            console.log(err);
        });
}