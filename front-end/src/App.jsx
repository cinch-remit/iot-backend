import React, {useEffect, Suspense, lazy} from "react";
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import Footer from "./components/footer/Footer";
import {useDispatch, useSelector} from "react-redux";
import HeaderMargin from "./components/header-margin/header-margin";
import Loading from "./components/loading/Loading";
import {doSetBuildInfo, doSetShowWelcomeMessage} from "./actions/info/appinfoActions";
import LandingPage from "./pages/LandingPage";
import LoggedInNavbar from "./components/navbar/LoggedInNavbar";
import {client} from "./sockjs/sockjs-config";
import {loadDevice} from "./actions/device/deviceActions";
import Navbar from "./components/navbar/Navbar";
import {loadDashboards} from "./actions/dashboard/dashboardActions";

const loggedInLandingPage = lazy(() => import('./pages/LoggedInLandingPage'));
const devicePage = lazy(() => import('./pages/device/DevicePage'));
const registerPage = lazy(() => import('./pages/register/Register'));
const confirmUserPage = lazy(() => import('./pages/ConfirmRegistration'));
const resendVerificationCodePage = lazy(() => import('./pages/register/ResendVerificationCode'));
const forgotPasswordPage = lazy(() => import('./pages/forgot/ForgotPasswordPage'));
const dashboardPage = lazy(() => import('./pages/dashboard/DashboardPage'));
const errorPage = lazy(() => import('./pages/error/Error'))

const App = () => {
    const dispatch = useDispatch();

    const { loggedIn, user } = useSelector(state => state.user);
    const { buildInfo } = useSelector(state => state.appInfo);

    const DEVICE_ADDED = "Device added";
    const READING_ADDED = "Reading added";
    const DASHBOARD_ADDED = "Dashboard added";

    useEffect(() => {
        if (!buildInfo.build) {
            dispatch(doSetBuildInfo())
        }
    },
        // eslint-disable-next-line
        [buildInfo])

    useEffect(() => {
        if (loggedIn) {
            dispatch(doSetShowWelcomeMessage(true));
        }
        // eslint-disable-next-line
    }, [loggedIn]);

    client.onConnect = () => {
        client.publish({destination: '/swns/start'});
        client.subscribe(`/user/notification/${user.id}`, (message) => {
            if (message.body) {
                const bodyInJson = JSON.parse(message.body);
                if (bodyInJson.type === DEVICE_ADDED || bodyInJson.type === READING_ADDED) {
                    dispatch(loadDevice(bodyInJson.details.deviceId))
                }

                if (bodyInJson.type === DASHBOARD_ADDED) {
                    dispatch(loadDashboards());
                }
            }
        })
    }

    client.onStompError = (frame) => {
        console.log(frame);
    }

    useEffect(() => {
        if (loggedIn && !client.connected) {
            client.activate();
        }
    }, [loggedIn])

  return (
    <Router>
        <Suspense fallback={<div>
            <HeaderMargin marginWithUnit={'24rem'}/>
            <div style={{
                width: '100%',
                textAlign: 'center',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center'
            }}>
                <div style={{
                    padding: '4rem'
                }}>
                    <Loading/>

                </div>
                <div>
                    <h2>
                        loading...
                    </h2>
                </div>
                <HeaderMargin marginWithUnit={'20rem'}/>
            </div>
        </div>}>
            {
                loggedIn ?
                    <LoggedInNavbar/>
                    :
                   <Navbar/>
            }

        <Switch>
            <Route path={'/'} exact>
                {loggedIn ? <Redirect to={'/home'}/> : <LandingPage/>}
            </Route>

            <Route path={'/register'} exact component={registerPage}/>
            <Route path={'/home'} exact component={loggedInLandingPage}/>
            <Route path={'/device/:deviceId'} component={devicePage} exact/>
            <Route path={'/confirm/:code'} component={confirmUserPage} exact/>
            <Route path={'/resendvercode'} component={resendVerificationCodePage} exact/>
            <Route path={'/forgot'} component={forgotPasswordPage} exact/>
            <Route path={'/dashboards'} component={dashboardPage} exact/>
            <Route component={errorPage}/>
        </Switch>
            {
                loggedIn ?
                    <Footer/>
                    :
                    null
            }
        </Suspense>
    </Router>
  );
}

export default App;
