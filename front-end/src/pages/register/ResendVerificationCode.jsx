import React, {useState} from "react";
import {defaultHeaders, doRequest} from "../../httpClient";
import {FAILED, LOADING, SUCCESS} from "../../actions/user/userActions";
import LoadingForPage from "../../components/loading/LoadingForPage";
import {Link} from "react-router-dom";
import {validateEmail} from "./Register";

const ResendVerificationCode = () => {
    const [email, setEmail] = useState('');
    const [status, setStatus] = useState('');
    const [errorMessage, setErrorMessage] = useState('');

    const resend = () => {

        if (!validateEmail(email)) {
            return;
        }
        const settings = {
            method: "PUT",
            headers: {
                ...defaultHeaders,
                email: email
            }
        }

        doRequest(`/userops/request-verification-code`, settings)
            .then(res => {
                if (res.ok) {
                    setStatus(SUCCESS)
                } else {
                    throw res;
                }
            })
            .catch(err => {
                console.log(err);
                setStatus(FAILED);

                err.json()
                    .then(data => {
                        setErrorMessage(data.message)
                    })
            })
    }

    const resetOnEnter = event => {
        if (event.key === 'Enter') {
            resend();
        }
    }

    if (status === LOADING) {
        return <LoadingForPage message={'one moment please...'}/>
    }

    if (status === FAILED) {
        return <div className={'mt5 tc'}>
            <main className="pa4 black-80">
                <div className="measure center">
                    <header className="tc ph4">
                        <h1 className="f3 f2-m f1-l fw2 black-90 mv3 red">
                            Something went wrong
                        </h1>
                        <h2 className="f5 f4-m f3-l fw2 black-50 mt0 lh-copy light-red">
                            {errorMessage}
                        </h2>
                    </header>

                    <div className="tc">
                        {
                            errorMessage === 'User already verified, login instead' ?
                                <Link
                                    className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib ttu tracked link black"
                                    to={'/login'}>
                                    login
                                </Link>
                                :
                                <p className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib ttu tracked link black"
                                   onClick={() => setStatus('')}>
                                    request new code
                                </p>
                        }
                    </div>

                </div>
            </main>
        </div>
    }

    if (status === SUCCESS) {
        return <div className={'mt5 tc'}>
            <main className="pa4 black-80">
                <div className="measure center">
                    <header className="tc ph4">
                        <h1 className="f3 f2-m f1-l fw2 black-90 mv3">
                            If {email} exists, we'll send an email with a new code.
                        </h1>
                    </header>

                    <div className="tc mt4">
                        <a className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer black f6 dib ttu tracked link"
                           href={'/login'}>
                            log in
                        </a>
                    </div>

                </div>
            </main>
        </div>
    }

    return (
        <div className={'mt5'} onKeyPress={resetOnEnter}>
            <main className="pa4 black-80">
                <div className="measure center">
                    <fieldset id="sign_up" className="ba b--transparent ph0 mh0">
                        <legend className="f4 fw6 ph0 mh0">Resend registration code</legend>
                        <div className="mt3">
                            <label className="db fw6 lh-copy f6" htmlFor="email-address">Email</label>
                            <input className="pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100"
                                   type="email" name="email-address" id="email-address"
                                   onChange={event => setEmail(event.target.value)}/>
                        </div>
                    </fieldset>

                    <div className="tc">
                        <p className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib ttu tracked"
                           onClick={resend}>
                            resend
                        </p>
                    </div>

                </div>
            </main>
        </div>
    )
}

export default ResendVerificationCode;