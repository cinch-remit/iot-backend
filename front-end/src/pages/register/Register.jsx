import React, {useState} from "react";
import HeaderMargin from "../../components/header-margin/header-margin";
import {Link} from "react-router-dom";
import {FAILED, LOADING, SUCCESS} from "../../actions/user/userActions";
import {defaultHeaders, doRequest} from "../../httpClient";
import Loading from "../../components/loading/Loading";

export const validateEmail = (email) => {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
};

export const validatePasswords = (password, passwordRepeat) => {
    return (
        /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/.test(password) &&
        /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/.test(passwordRepeat) && (password === passwordRepeat)
    );
};

const Register = () => {

    const [email, setEmail] = useState('');
    const [emailError, setEmailError] = useState(false);

    const [password, setPassword] = useState('');
    const [passwordRepeat, setPasswordRepeat] = useState('');
    const [passwordError, setPasswordError] = useState(false);

    const [passwordErrorMessage, setPasswordErrorMessage] = useState('');
    const [failedMessage, setFailedMessage] = useState('');


    const [status, setStatus] = useState('');


    const handleEmailChange = event => {
        setEmailError(false);
        setEmail(event.target.value);
    }

    const handlePasswordChange = event => {
        setPasswordError(false);
        setPassword(event.target.value);
    }

    const handlePasswordRepeatChange = event => {
        setPasswordError(false);
        setPasswordRepeat(event.target.value);
    }

    const reset = () => {
        setStatus('');
    }

    const register = () => {
        if (!validateEmail(email)) {
            setEmailError(true);
            return;
        }
        if (password !== passwordRepeat) {
            setPasswordErrorMessage('passwords do not match');
            setPasswordError(true);
            return;
        }

        if (!validatePasswords(password, passwordRepeat)) {
            setPasswordErrorMessage('please select a stronger password');
            setPasswordError(true);
            return;
        }

        setStatus(LOADING);

        const settings = {
            method: "POST",
            headers: defaultHeaders,
            body: JSON.stringify({
                email: email,
                password: btoa(password)
            })
        }

        doRequest(`/userops/register`, settings)
            .then(res => {
                if (res.ok) {
                    setStatus(SUCCESS);
                }else {
                    throw res;
                }
            })
            .catch(err => {
                setStatus(FAILED);
                err.json()
                    .then(info => {
                        setFailedMessage(info.message)
                    })
            });
    }

    if (status === LOADING) {
        return <div className={'mt7 tc'}>
            <Loading>
                <p className={'ttu'}>one moment please...</p>
            </Loading>

            <HeaderMargin marginWithUnit={'40rem'}/>
        </div>
    }


    if (status === FAILED) {
        return  <div className={'mt7 tc'}>
            <main className="pa4 black-80">
                <div className="measure center">
                    <header className="tc ph4">
                        <h1 className="f3 f2-m f1-l fw2 black-90 mv3 red">
                            Something went wrong
                        </h1>
                        <h2 className="f5 f4-m f3-l fw2 black-50 mt0 lh-copy light-red">
                            {failedMessage}
                        </h2>
                    </header>

                    <div className="tc">
                        <p className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib ttu tracked" onClick={reset}>
                            try again
                        </p>
                    </div>

                    <div className="tc mt3">
                        <a href="/login" className="f6 link dim black db ttu tracked blue mb2">login instead</a>
                        <Link to={'/forgot'} className="f6 link dim black db">forgot your password?</Link>
                    </div>
                </div>
            </main>
        </div>
    }

    if (status === SUCCESS) {
        return  <div className={'mt7 tc'}>
            <main className="pa4 black-80">
                <div className="measure center">
                    <header className="tc ph4">
                        <h1 className="f3 f2-m f1-l fw2 black-90 mv3 green">
                            Congratulations.
                        </h1>
                        <h2 className="f5 f4-m f3-l fw2 black-50 mt0 lh-copy black-50">
                            Please check your email for verification details.
                        </h2>
                    </header>

                    <div className="tc">
                        <a className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer black f6 dib ttu tracked link" href={'/login'}>
                            log in
                        </a>
                    </div>

                </div>
            </main>
        </div>
    }

    return (
        <div className={'mt5'}>
            <main className="pa4 black-80">
                <div className="measure center">
                    <fieldset id="sign_up" className="ba b--transparent ph0 mh0">
                        <legend className="f4 fw6 ph0 mh0">Register</legend>
                            <div className="mt3">
                                <label className="db fw6 lh-copy f6" htmlFor="email-address">Email</label>
                                <input className="pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100"
                                       type="email" name="email-address" id="email-address" onChange={handleEmailChange}/>
                            </div>
                            <div className="mv3">
                                <label className="db fw6 lh-copy f6" htmlFor="password">Password</label>
                                <input className="b pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100"
                                       type="password" name="password" id="password" onChange={handlePasswordChange}/>
                            </div>

                        <div className="mv3">
                            <label className="db fw6 lh-copy f6" htmlFor="password-repeat">Password repeat</label>
                            <input className="b pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100"
                                   type="password" name="password-repeat" id="password-repeat" onChange={handlePasswordRepeatChange}/>
                        </div>

                    </fieldset>
                    <div className="tc">
                        <p className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib ttu tracked" onClick={register}>
                            register
                        </p>
                    </div>

                    {
                        emailError ?
                            <div className={'tc'}>
                                <p className={'dark-red fw3 f6'}>invalid email</p>
                            </div>
                            :
                            null
                    }

                    {
                        passwordError ?
                            <div className={'tc red fw3 f6'}>
                                <p>
                                    {passwordErrorMessage}
                                </p>
                            </div>
                            :
                            null
                    }

                    <div className="tc mt3">
                        <a href="/login" className="f6 link dim black db ttu tracked blue mb2">login instead</a>
                        <Link to={'/forgot'} className="f6 link dim black db">forgot your password?</Link>
                    </div>
                </div>


            </main>
        </div>
    )
};
export default Register;