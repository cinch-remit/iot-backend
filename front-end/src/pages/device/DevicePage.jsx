import React, {Fragment, useEffect, useState} from "react";
import {Redirect, useParams} from 'react-router-dom';
import {doSetUser, FAILED, LOADING, SUCCESS} from "../../actions/user/userActions";
import Loading from "../../components/loading/Loading";

import {Chip, makeStyles, Tooltip} from "@material-ui/core";
import ChartContainer from "../../components/chart/ChartContainer";
import IotLineSeries from "../../components/chart/IotLineSeries";
import HeaderMargin from "../../components/header-margin/header-margin";

import {useDispatch, useSelector} from "react-redux";
import {loadDevices} from "../../actions/device/deviceActions";
import DeviceBreadcrumb from "../../components/Breadcrumbs/DeviceBreadcrumb";
import MetricsContainer from "../../components/metrics/MetricsContainer";
import ReadingContainer from "../../components/reading/ReadingContainer";

import Save from '@material-ui/icons/Save'
import TransitionModal from "../../components/modals/TransitionModal";
import SaveDashboard from "../../components/dashboard/SaveDashboard";
import {constructData} from "../../components/dashboards/dashboardUtil/DashboardUtil";


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
        '& > *': {
            margin: theme.spacing(0.5),
        },
    },
}));

const DevicePage = () => {

    const { user, loggedIn, status } = useSelector(state => state.user);

    const classes = useStyles();

    const {devices} = useSelector(state => state.devices)

    const {deviceId} = useParams();
    const [device, setDevice] = useState({});

    const [deviceStatus, setDeviceStatus] = useState(LOADING);
    const [selectedKey, setSelectedKey] = useState('');
    const [keySelected, setKeySelected] = useState(false);
    const [metricsForKeySelected, setMetricsForKeySelected] = useState([]);

    const [selectedMetric, setSelectedMetric] = useState('');
    const [metricSelected, setMetricSelected] = useState(false);
    const [dataForSelectedMetric, setDataForSelectedMetric] = useState([]);


    const [selectedXAxis, setSelectedXAxis] = useState('');
    const [xAxisSelected, setXAxisSelected] = useState(false);

    const [selectedYAxis, setSelectedYAxis] = useState(undefined);
    const [yAxisSelected, setYAxisSelected] = useState(false);


    const [modalState, setModalState] = useState(false);

    const [dashboardName, setDashboardName] = useState('');

    const handleKeySelect = key => {
        setSelectedYAxis('');
        setYAxisSelected(false);

        setSelectedXAxis('');
        setXAxisSelected(false);

        setMetricSelected(false);
        setSelectedMetric('');
        setDataForSelectedMetric([]);

        if (keySelected && selectedKey === key) {
            setKeySelected(false);
            setSelectedKey('');
            setMetricsForKeySelected([]);
            return;
        }
        setSelectedKey(key);
        setKeySelected(true);

        setMetricsForKeySelected(Object.keys(device.data[key]));
    }


    const handleMetricSelect = metric => {
        setSelectedYAxis('');
        setYAxisSelected(false);

        setSelectedXAxis('');
        setXAxisSelected(false);

        if (metricSelected && selectedMetric === metric) {
            setMetricSelected(false);
            setSelectedMetric('');
            setDataForSelectedMetric([]);
            return;
        }

        setSelectedMetric(metric);
        setMetricSelected(true);

        setDataForSelectedMetric(device.data[selectedKey][metric])
    }

    useEffect(() => {
        if (metricSelected && xAxisSelected && yAxisSelected) {
            setDataForSelectedMetric(device.data[selectedKey][selectedMetric])
            setConstructedData(constructData(selectedXAxis, selectedYAxis, device.data[selectedKey][selectedMetric]))
        }
    },
        // eslint-disable-next-line
        [device]);

    const handleXAxisSelect = xAxis => {
        if (xAxisSelected && selectedXAxis === xAxis) {
            setSelectedXAxis('');
            setXAxisSelected(false);

            return;
        }

        setSelectedXAxis(xAxis);
        setXAxisSelected(true);
    }

    const handleYAxisSelect = yAxis => {
        if (yAxisSelected && selectedYAxis === yAxis) {
            setSelectedYAxis('');
            setYAxisSelected(false);

            return;
        }

        setSelectedYAxis(yAxis);
        setYAxisSelected(true);
    }

    const [constructedData, setConstructedData] = useState([]);

    const dispatch = useDispatch();

    useEffect(() => {
            if (devices.length === 0) {
                dispatch(loadDevices());
            } else {
                const selectedDevice = [...devices].filter(d => d.deviceID === deviceId);
                if (selectedDevice && selectedDevice.length > 0 && selectedDevice[0].data) {
                    setDevice(selectedDevice[0])
                    setDeviceStatus(SUCCESS)
                }
            }
        },
        // eslint-disable-next-line
        [devices]);

    useEffect(() => {
        if (xAxisSelected && yAxisSelected) {
            setConstructedData(constructData(selectedXAxis, selectedYAxis, dataForSelectedMetric));
        }
    },
        // eslint-disable-next-line
        [selectedXAxis, selectedYAxis, xAxisSelected, yAxisSelected])

    useEffect(() => {
        if (!loggedIn) {
            dispatch(doSetUser())
        }
    },
        // eslint-disable-next-line
        [user]);



    if (status === FAILED) {
        return <Redirect to={'/'}/>
    }

    if (deviceStatus === LOADING) {
        return (
            <Loading>
                <p>...</p>
            </Loading>
        );
    }
    return (
        <div className={'tc'}>
            <div>
                <DeviceBreadcrumb currentLocation={`/device/${deviceId}`} links={[{link: '/home', display: 'home'}, {link: `/device/${deviceId}`, display: `${deviceId}`}]}/>
            </div>
            <h4 className={'tracked'}>{`Device Name: ${device.deviceID}`}</h4>

            {
                device.data && Object.keys(device.data).length > 0  ?
                    <Fragment>
                        <div>
                            <p className={'fw3 f6'}><small
                                className={'tracked ttu'}>{`Available readings for ${device.deviceID}`}</small></p>
                            <hr className="mw3 bb bw1 b--black-10"/>
                            <div className={classes.root}>
                                <ReadingContainer readings={Object.keys(device.data)} selectedKey={selectedKey} handleKeySelect={handleKeySelect}/>
                            </div>
                        </div>

                        {
                            keySelected ?
                                <Fragment>
                                    <div className={'mb3 ma3'}>
                                        <p className={'fw3 f6'}><small
                                            className={'tracked ttu'}>{`Available metrics for ${selectedKey}`}</small>
                                        </p>
                                        <MetricsContainer metrics={metricsForKeySelected} selectedMetric={selectedMetric} handleMetricSelect={handleMetricSelect}/>
                                    </div>
                                    {
                                        selectedMetric ?
                                            <Fragment>

                                                {
                                                    xAxisSelected ?
                                                        <div>
                                                            <Chip clickable color={'default'} variant={"outlined"}
                                                                  className={'ma1 f6 fw6 tracked'}
                                                                  onClick={() => handleXAxisSelect(selectedXAxis)}
                                                                  label={`x axis: ${selectedXAxis === 'value' ? selectedKey : selectedXAxis}`}/>
                                                        </div>
                                                        :
                                                        <div>
                                                            <header className="tc mt3">
                                                                <h2 className="f5 f4-m f3-l fw2 black-50 mt0 lh-copy">
                                                                    Select data key for x axis
                                                                </h2>
                                                            </header>
                                                            <div className={classes.root}>
                                                                {
                                                                    Object.keys(dataForSelectedMetric[0]).map((data, i) =>
                                                                        <Chip
                                                                            key={i} clickable color={'default'}
                                                                            variant={"outlined"}
                                                                            className={'ma1 f6 fw6 tracked'}
                                                                            onClick={() => handleXAxisSelect(data)}
                                                                            label={data === 'value' ? selectedKey : data}/>)
                                                                }
                                                            </div>
                                                        </div>

                                                }

                                                {
                                                    yAxisSelected ?
                                                        <div className={'mt2'}>
                                                            <Chip clickable color={'default'} variant={"outlined"}
                                                                  className={'ma1 f6 fw6 tracked'}
                                                                  onClick={() => handleYAxisSelect(selectedYAxis)}
                                                                  label={`y axis: ${selectedYAxis === 'value' ? selectedKey : selectedYAxis}`}/>
                                                        </div>
                                                        :
                                                        <div>
                                                            <header className="tc mt3">
                                                                <h2 className="f5 f4-m f3-l fw2 black-50 mt0 lh-copy">
                                                                    Select data key for y axis
                                                                </h2>
                                                            </header>
                                                            <div className={classes.root}>
                                                                {
                                                                    Object.keys(dataForSelectedMetric[0]).filter((selected) => selected !== selectedXAxis).map((data, i) =>
                                                                        <Chip key={i} clickable color={'default'}
                                                                              variant={"outlined"}
                                                                              className={'ma1 f6 fw6 tracked'}
                                                                              onClick={() => handleYAxisSelect(data)}
                                                                              label={data === 'value' ? selectedKey : data}/>)
                                                                }
                                                            </div>
                                                        </div>

                                                }

                                                {
                                                    yAxisSelected && xAxisSelected ?
                                                        <Fragment>
                                                            <div style={{
                                                                display: 'flex',
                                                                flexDirection: 'row',
                                                                justifyContent: 'center',
                                                                marginTop: '1rem',
                                                                cursor: 'pointer'
                                                            }}>
                                                                <Tooltip title={'save these settings as a dashboard'}>
                                                                    <Save fontSize={'default'} color={'primary'} style={{
                                                                        marginTop: '10px'
                                                                    }} onClick={() => setModalState(true)}/>
                                                                </Tooltip>
                                                            </div>
                                                        <ChartContainer>
                                                            <IotLineSeries
                                                                xTitle={selectedXAxis === 'value' ? selectedKey : selectedXAxis}
                                                                yTitle={selectedYAxis === 'value' ? selectedKey : selectedYAxis}
                                                                data={constructedData}
                                                                xType={selectedXAxis === 'timeOfReading' ? 'time' : 'ordinal'}
                                                                yType={selectedYAxis === 'timeOfReading' ? 'time' : 'ordinal'}
                                                                xMetric={selectedXAxis === 'value' ? selectedKey : selectedXAxis}
                                                                yMetric={selectedYAxis === 'value' ? selectedKey : selectedYAxis}
                                                            />
                                                        </ChartContainer>
                                                        </Fragment>
                                                        :
                                                        null
                                                }

                                            </Fragment>
                                            :
                                            <div>
                                                <header className="tc mt3">
                                                    <h2 className="f5 f4-m f3-l fw2 black-50 mt0 lh-copy">
                                                        Select a metric to visualize data
                                                    </h2>
                                                </header>
                                            </div>
                                    }

                                </Fragment>
                                :
                                <div>
                                    <header className="tc mt3">
                                        <h2 className="f5 f4-m f3-l fw2 black-50 mt0 lh-copy">
                                            Select a reading to visualize data
                                        </h2>
                                    </header>
                                </div>
                        }
                    </Fragment>
                    :
                    <div>
                        <header className="tc mt3">
                            <p className="fw3 f6 tracked">
                                {`No data for device yet`}
                            </p>

                            <Loading>
                                <p className={'fw3 f6'}><small
                                    className={'tracked ttu blue'}>{`Listening for new data...`}</small></p>
                            </Loading>
                        </header>

                        <HeaderMargin marginWithUnit={'20rem'}/>
                    </div>
            }


            <TransitionModal modalState={modalState} setModalState={setModalState}>
                <SaveDashboard selectedKey={selectedKey}
                               dashboardName={dashboardName}
                               setDashboardName={setDashboardName}
                               selectedMetric={selectedMetric}
                               deviceId={device.deviceID}
                               xAxis={selectedXAxis}
                               yAxis={selectedYAxis}
                               handleModal={setModalState}
                />
            </TransitionModal>
        </div>
    )
};

export default DevicePage;