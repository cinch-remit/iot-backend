import React, {useEffect} from "react";
import DevicesContainer from "../components/devices/DevicesContainer";
import IotSnackBar from "../components/notification/SnackBar";
import {useDispatch, useSelector} from "react-redux";
import {doSetShowWelcomeMessage} from "../actions/info/appinfoActions";
import {doLoadNotifications, doSetUser} from "../actions/user/userActions";
import {loadDevices} from "../actions/device/deviceActions";
import {loadDashboards} from "../actions/dashboard/dashboardActions";

const LoggedInLandingPage = () => {
    const {user, notifications} = useSelector(state => state.user);
    const {welcomeMessage} = useSelector(state => state.appInfo);
    const {devices} = useSelector(state => state.devices);
    const { dashboards } = useSelector(state => state.dashboard)


    const dispatch = useDispatch();

    const handleWelcomeMessage = val => {
        dispatch(doSetShowWelcomeMessage(val))
    }

    useEffect(() => {
        if (devices.length === 0) {
            dispatch(loadDevices());
        }
    },
        // eslint-disable-next-line
        [])

    useEffect(() => {
        if (notifications.length === 0) {
            dispatch(doLoadNotifications())
        }
    },
        // eslint-disable-next-line
        [])

    useEffect(() => {
            if (dashboards.length === 0) {
                dispatch(loadDashboards())
            }
        },
        // eslint-disable-next-line
        [])


    useEffect(() => {
            if (!user.email) {
                dispatch(doSetUser())
            }
        },
        // eslint-disable-next-line
        [user]);

    return (
        <div>
            <IotSnackBar setOpen={handleWelcomeMessage} open={welcomeMessage} message={`Welcome back ${user.email}`}/>
            <DevicesContainer/>
        </div>
    )
};

export default LoggedInLandingPage;