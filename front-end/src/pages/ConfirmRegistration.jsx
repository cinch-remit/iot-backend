import React, {useEffect, useState} from "react";
import {Link, useParams} from "react-router-dom";
import {defaultHeaders, doRequest} from "../httpClient";
import {FAILED, LOADING, SUCCESS} from "../actions/user/userActions";
import Loading from "../components/loading/Loading";
import HeaderMargin from "../components/header-margin/header-margin";

const ConfirmRegistration = () => {
    const { code } = useParams();
    const [status, setStatus] = useState(LOADING);
    const [errorMessage, setErrorMessage] = useState('');

    const confirmUser = () => {
        if (code) {
            const settings = {
                method: "PUT",
                headers: defaultHeaders
            }

            doRequest(`/userops/confirm/${code}`, settings)
                .then(res => {
                    if (res.ok) {
                        setStatus(SUCCESS)
                    }else {
                        throw res
                    }
                })
                .catch(err => {
                    setStatus(FAILED)
                    err.json()
                        .then(data => {
                            setErrorMessage(data.message);
                        })
                })
        }
    }

    useEffect(() => confirmUser(),
        // eslint-disable-next-line
        []);

    if (status === FAILED) {
        return  <div className={'mt5 tc'}>
            <main className="pa4 black-80">
                <div className="measure center">
                    <header className="tc ph4">
                        <h1 className="f3 f2-m f1-l fw2 black-90 mv3 red">
                            Something went wrong
                        </h1>
                        <h2 className="f5 f4-m f3-l fw2 black-50 mt0 lh-copy light-red">
                            {errorMessage}
                        </h2>
                    </header>

                    <div className="tc">
                        <Link className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib ttu tracked link black" to={'/resendvercode'}>
                            request new code
                        </Link>
                    </div>

                </div>
            </main>
        </div>
    }

    if (status === SUCCESS) {
        return  <div className={'mt5 tc'}>
            <main className="pa4 black-80">
                <div className="measure center">
                    <header className="tc ph4">
                        <h1 className="f3 f2-m f1-l fw2 black-90 mv3 green">
                            Congratulations.
                        </h1>
                        <h2 className="f5 f4-m f3-l fw2 black-50 mt0 lh-copy black-50">
                            We've verified your email.
                        </h2>
                    </header>

                    <div className="tc">
                        <a className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer black f6 dib ttu tracked link" href={'/login'}>
                            log in
                        </a>
                    </div>

                </div>
            </main>
        </div>
    }

    return <div className={'mt5 tc'}>
        <Loading>
            <p className={'ttu fw3 f6'}>one moment please...</p>
        </Loading>

        <HeaderMargin marginWithUnit={'40rem'}/>
    </div>
}

export default ConfirmRegistration;