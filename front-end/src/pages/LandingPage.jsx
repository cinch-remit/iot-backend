import React from "react";
import {Link} from "react-router-dom";
import logo from '../assets/logo.png'

const LandingPage = () => {
    return (
        <section className="flex-ns vh-100 items-center">
            <div className="mw6 ph5">
                <img src={logo} alt={'iot-backend logo'}/>
            </div>
            <div className="tc tl-ns ph3">
                <h1 className="f3 f1-l fw2 mb3 mt4 mt0-ns">Iot Backend App</h1>
                <h2 className="f5 f3-l fw1 mb4 mb5-l lh-title">Visualize data from your iot device.</h2>

                <a className="f6 link dim br1 ba ph4 pv2 mb2 dib black ma1 ttu b--blue blue" href="/login">log in</a>
                <Link className="f6 link dim br1 ba ph4 pv2 mb2 dib black ma1 ttu white bg-blue b--blue" to={'/register'}>register</Link>
            </div>
        </section>
    )
}
export default LandingPage;