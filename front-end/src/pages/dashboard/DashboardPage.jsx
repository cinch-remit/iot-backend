import React, {useEffect, useState, Fragment} from "react";
import DashboardsContainer from "../../components/dashboards/DashboardsContainer";
import {doSetUser} from "../../actions/user/userActions";
import {useDispatch, useSelector} from "react-redux";
import {loadDashboards} from "../../actions/dashboard/dashboardActions";
import {loadDevices} from "../../actions/device/deviceActions";
import {constructData} from "../../components/dashboards/dashboardUtil/DashboardUtil";
import IotLineSeries from "../../components/chart/IotLineSeries";
import ChartContainer from "../../components/chart/ChartContainer";

const DashboardPage = () => {

    const {user} = useSelector(state => state.user);
    const {devices} = useSelector(state => state.devices)
    const {dashboards} = useSelector(state => state.dashboard);
    const [selectedDashboard, setSelectedDashboard] = useState({});
    const [dashboardDevice, setDashboardDevice] = useState({});

    const selectDashboard = dashboardName => {
        const selected = dashboards.filter(dashboard => dashboard.name === dashboardName);

        if (selected.length > 0) {
            setSelectedDashboard(selected[0]);
            const selectedDevice = devices.filter(device => selected[0].deviceId === device.deviceID);

            if (selectedDevice.length > 0) {
                setDashboardDevice(selectedDevice[0]);
            }
        }
    }

    const dispatch = useDispatch();

    useEffect(() => {
            if (!user.email) {
                dispatch(doSetUser())
            }
        },
        // eslint-disable-next-line
        [user]);

    useEffect(() => {
            if (dashboards.length === 0) {
                dispatch(loadDashboards())
            }
        },
        // eslint-disable-next-line
        []);

    useEffect(() => {
            if (devices.length === 0) {
                dispatch(loadDevices())
            }
        },
        // eslint-disable-next-line
        []);

    const [constructedData, setConstructedData] = useState([]);


    useEffect(() => {
        if (dashboardDevice.data) {
            setConstructedData(constructData(selectedDashboard.xaxis, selectedDashboard.yaxis, dashboardDevice.data[selectedDashboard.typeOfReading][selectedDashboard.metric]));
        }
    }, [dashboardDevice, selectedDashboard]);

    return (
        <div className={'tc'}>
            <DashboardsContainer dashboards={dashboards} selectDashboard={selectDashboard}/>
            {
                dashboardDevice.data ?
                    <Fragment>
                        <div className={'tc tracked fw3 f6 ttu'}>
                            <p>
                                selected dashboard: <span className={'blue'}>{selectedDashboard.name}</span>
                            </p>
                        </div>
                    <ChartContainer>
                        <IotLineSeries
                            xTitle={selectedDashboard.xaxis === 'value' ? selectedDashboard.typeOfReading : selectedDashboard.xaxis}
                            yTitle={selectedDashboard.yaxis === 'value' ? selectedDashboard.typeOfReading : selectedDashboard.yaxis}
                            data={constructedData}
                            xType={selectedDashboard.xaxis === 'timeOfReading' ? 'time' : 'ordinal'}
                            yType={selectedDashboard.yaxis === 'timeOfReading' ? 'time' : 'ordinal'}
                            xMetric={selectedDashboard.xaxis === 'value' ? selectedDashboard.typeOfReading : selectedDashboard.xaxis}
                            yMetric={selectedDashboard.yaxis === 'value' ? selectedDashboard.typeOfReading : selectedDashboard.yaxis}
                        />
                    </ChartContainer>
                        <div className={'tc ttu tracked'}>
                            <p>
                                {selectedDashboard.name}
                            </p>
                        </div>
                    </Fragment>
                    :
                    null
            }
        </div>
    )
}

export default DashboardPage;