import React from "react";

const Greeting = () => {
    return (
        <header className="bg-white black-80 tc pv4 avenir">
            <h2 className="mt2 mb0 f6 fw4 ttu tracked">Welcome back</h2>
        </header>
    )
}

export default Greeting;