import React, {useState} from "react";
import {Button, TextField} from "@material-ui/core";
import {FAILED, LOADING, SUCCESS} from "../../actions/user/userActions";
import {defaultHeaders, doRequest} from "../../httpClient";
import ApiCallFailed from "../api-response/ApiCallFailed";
import ApiCallSuccess from "../api-response/ApiCallSuccess";
import {Link} from "react-router-dom";
import Loading from "../loading/Loading";

const SaveDashboard = ({ deviceId, selectedKey, selectedMetric, xAxis, yAxis, handleModal }) => {

    const [dashboardName,setDashboardName ] = useState('');
    const [status, setStatus] = useState('');
    const [errorMessage, setErrorMessage] = useState('');

    const save = () => {
        setStatus(LOADING);

        const settings = {
            method: 'POST',
            headers: defaultHeaders,
            body: JSON.stringify({
                name: dashboardName,
                deviceId: deviceId,
                typeOfReading: selectedKey,
                metric: selectedMetric,
                xaxis: xAxis,
                yaxis:yAxis
            })
        };

        doRequest(`/v1/dashboard`, settings)
            .then(res => {
                if (res.ok) {
                    return res;
                }
                throw res;
            })
            .then(data => {
                setStatus(SUCCESS);
            })
            .catch(err => {
                setStatus(FAILED);
                err.json()
                    .then(data => setErrorMessage(data.message));
            })
    }

    if (status === LOADING) {
        return <div className={'tc'}>
            <Loading>
                <p>one moment please...</p>
            </Loading>
        </div>
    }
    if (status === FAILED) {
        return <ApiCallFailed mainErrorMessage={'Could not save dashboard'} errorCause={errorMessage}>
            <div className="tc">
                <p className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib ttu tracked" onClick={() => {
                    handleModal(false);
                }}>
                    close
                </p>
            </div>
        </ApiCallFailed>
    }

    if (status === SUCCESS) {
        return <ApiCallSuccess mainMessage={`Dashboard has been saved..`}>
            <div className="tc">
                <Link className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer black f6 dib ttu tracked link" to={'/dashboards'}>
                    see your dashboards
                </Link>
            </div>
        </ApiCallSuccess>
    }
    return (
        <div>
            <p className={'fw3 f6 tracked'}>Save settings as dashboard...</p>
            <div className={'tc center'}>
                <TextField placeholder={'dashboard name'} className={'fw3 f6'} onChange={event => setDashboardName(event.target.value)} value={dashboardName}/>
                <p className={'fw3 f6 mb2'}><small className={'black-50'}>device</small>: <span className={'tracked'}>{deviceId}</span></p>
                <p className={'fw3 f6 mb2'}><small className={'black-50'}>type</small>: <span className={'tracked'}>{selectedKey}</span></p>
                <p className={'fw3 f6 mb2'}><small className={'black-50'}>metric</small>: <span className={'tracked'}>{selectedMetric}</span></p>
                <p className={'fw3 f6 mb2'}><small className={'black-50'}>x-axis</small>: <span className={'tracked'}>{xAxis  === 'value' ? selectedKey : xAxis}</span></p>
                <p className={'fw3 f6 mb2'}><small className={'black-50'}>y-axis</small>: <span className={'tracked'}>{yAxis  === 'value' ? selectedKey : yAxis}</span></p>
            </div>

            <div className={'tc mt3'}>
                <Button variant={'contained'} color="primary" onClick={save} disabled={dashboardName === ''}>
                    save
                </Button>
            </div>
        </div>
    )
}

export default SaveDashboard;