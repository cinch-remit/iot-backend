import React from "react";
const HeaderMargin = ({ marginWithUnit}) => (
    <div style={{
        marginTop: marginWithUnit ? marginWithUnit : '4rem'
    }}/>
)
export default HeaderMargin;
