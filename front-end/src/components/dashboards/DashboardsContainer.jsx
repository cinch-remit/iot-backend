import React from "react";
import Loading from "../loading/Loading";
import DashboardCard from "./DashboardCard";

const DevicesContainer = ({ dashboards, selectDashboard }) => {
    return <div className={'tc'}>
        <h4 className={'tracked'}>{dashboards.length > 0 ? 'Your dashboards' : 'You have no saved dashboards'}</h4>

        {
            dashboards.length === 0 ?
                <Loading>
                    <p className={'fw3 f6'}><small
                        className={'tracked ttu blue'}>{`Listening for new data...`}</small></p>
                </Loading>
                :
                null
        }
        {
            dashboards.map((dashboard, i) => <DashboardCard dashboard={dashboard} key={i} selectDashboard={() => selectDashboard(dashboard.name)}/> )
        }

    </div>
};

export default DevicesContainer;