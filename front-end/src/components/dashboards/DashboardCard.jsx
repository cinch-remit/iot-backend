import React from "react";

const DashboardCard = ({ dashboard, selectDashboard }) => {

    const {
        name,
        deviceId,
        typeOfReading,
        metric,
        xaxis,
        yaxis
    } = dashboard;

    return (
        <div className={'dib ma2'} onClick={() => selectDashboard(true)}>
            <article className="mw5 center bg-white br3 pa3 pa2-ns mv3 ba b--black-10 pointer grow">
                <div className="tc">
                    <p className="f6 fw3 tracked">{name}</p>
                    <hr className="mw3 bb bw1 b--black-10"/>
                </div>

                <p className="lh-copy measure center f6 black-70 tracked">
                    <small className={'ttu black-40'}>{`device: `}</small>
                    {deviceId}
                </p>

                <p className="lh-copy measure center f6 black-70 tracked">
                    <small className={'ttu black-40'}>{`key: `}</small>
                    {typeOfReading}
                </p>

                <p className="lh-copy measure center f6 black-70 tracked">
                    <small className={'ttu black-40'}>{`metric: `}</small>
                    {metric}
                </p>

                <p className="lh-copy measure center f6 black-70 tracked">
                    <small className={'ttu black-40'}>{`x axis: `}</small>
                    {xaxis === 'value' ? typeOfReading : xaxis}
                </p>

                <p className="lh-copy measure center f6 black-70 tracked">
                    <small className={'ttu black-40'}>{`y axis: `} </small>
                    {yaxis === 'value' ? typeOfReading : yaxis}
                </p>
            </article>
        </div>
    )
}

export default DashboardCard;