export const constructData = (xAxis, yAxis,dfsm) => {
    return dfsm.map((data, i) => {

        let xData = data[xAxis];
        let yData = data[yAxis];

        if (xAxis === 'value') {
            xData = parseFloat(xData)
        }

        if (yAxis === 'value') {
            yData = parseFloat(yData)
        }

        if (xAxis === 'timeOfReading') {
            xData = new Date(xData)
        }

        if (yAxis === 'timeOfReading') {
            yData = new Date(yData)
        }

        return {x: xData, y: yData}
    });
}