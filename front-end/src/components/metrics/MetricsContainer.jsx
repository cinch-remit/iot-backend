import React, {Fragment} from "react";
import {Chip} from "@material-ui/core";

const MetricsContainer = ({ metrics, selectedMetric, handleMetricSelect }) => {
    const data = metrics.map((metric, i) => <Chip clickable color={'primary'}
                                                  variant={selectedMetric === metric ? "default" : "outlined"}
                                                  className={'ma1 f6 fw6 tracked'}
                                                  key={i}
                                                  onClick={() => handleMetricSelect(metric)}
                                                  label={metric}/>)

    return(
        <Fragment>
            {data}
        </Fragment>
    )
}

export default MetricsContainer;