import React from "react";
import {Link} from "react-router-dom";

const Navbar = () => {
    return (
        <nav className="db dt-l w-100 border-box pa3 ph5-l">
            <div className="db dtc-l v-mid w-100 w-75-l tc tr-l">
                        <Link className="link dim dark-gray f6 f5-l dib mr3 mr4-l ttu tracked" to={'/'}
                              title="Home">Home</Link>

                <a className="link dim dark-gray f6 f5-l dib ttu tracked" href={'/login'}
                   title="Login">Login</a>
            </div>
        </nav>
    )
}
export default Navbar;