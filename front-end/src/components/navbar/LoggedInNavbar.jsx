import React, {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";

const LoggedInNavbar = () => {
    const { user } = useSelector(state => state.user);
    const [apiRole, setApiRole] = useState(false);

    useEffect(() => {
        if (user.roles) {
            const roles = user.roles;
            setApiRole(roles.findIndex(role => role.toLowerCase() === 'api') >= 0);
        }
    }, [user]);

    return (
        <nav className="db dt-l w-100 border-box pa3 ph5-l">
            <Link className="db dtc-l v-mid mid-gray link dim w-100 w-25-l tc tl-l mb2 mb0-l" to={'/home'}>
                <img src="http://tachyons.io/img/logo.jpg" className="dib w2 h2 br-100" alt="Site Name"/>
            </Link>
            <div className="db dtc-l v-mid w-100 w-75-l tc tr-l">
                        <Link className="link dim dark-gray f6 f5-l dib mr3 mr4-l ttu tracked" to={'/home'}
                              title="Home">Home</Link>
                
                <Link className="link dim dark-gray f6 f5-l dib mr3 mr4-l ttu tracked" to={'/dashboards'}
                      title="Data">Dashboards</Link>
                {
                    apiRole ?
                        <a className="link dim dark-gray f6 f5-l dib mr3 mr4-l ttu tracked" href={'/swagger-ui.html'}
                           target={'_blank'} rel={'noreferrer'} title="API">API</a>
                        :
                        null
                }
                <Link className="link dim dark-gray f6 f5-l dib mr3 mr4-l ttu tracked" to={'/settings'}
                      title="Settings">Settings</Link>
                        <a className="link dim dark-gray f6 f5-l dib ttu tracked" href={'/logout'}
                           title="Logout">Logout</a>

            </div>
        </nav>
    )
}
export default LoggedInNavbar;