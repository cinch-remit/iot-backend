import React from "react";
import {useSelector} from "react-redux";
import DeviceCard from "./DeviceCard";
import Loading from "../loading/Loading";

const DevicesContainer = () => {
    const { devices } = useSelector(state => state.devices)
    return <div className={'tc'}>
        <h4 className={'tracked'}>{devices.length > 0 ? 'Your devices' : 'You have no registered devices'}</h4>

        {
            devices.length === 0 ?
                <Loading>
                    <p className={'fw3 f6'}><small
                        className={'tracked ttu blue'}>{`Listening for new data...`}</small></p>
                </Loading>
                :
                null
        }
            {
                devices.map((device, i) => <DeviceCard device={device} key={i}/> )
            }

    </div>
};

export default DevicesContainer;