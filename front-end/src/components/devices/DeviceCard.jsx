import React, {useState} from "react";
import moment from "moment";
import { Redirect } from 'react-router-dom';

const DeviceCard = ({ device }) => {

    const { deviceID, lastTransmission,  totalReadingsForDevice } = device;
    const [redirectToPage, setRedirectToPage] = useState(false);

    if (redirectToPage) {
        return <Redirect to={`/device/${deviceID}`}/>
    }
    return (
        <div className={'dib ma2'} onClick={() => setRedirectToPage(true)}>
        <article className="mw5 center bg-white br3 pa3 pa2-ns mv3 ba b--black-10 pointer grow">
            <div className="tc">
                    <p className="f6 fw3 tracked">{deviceID}</p>
                    <hr className="mw3 bb bw1 b--black-10"/>
            </div>
            {
                lastTransmission.includes('No Transmission') ?
                    <time className="lh-copy measure center f6 black-70 tracked">
                        <small>{lastTransmission}</small>
                    </time>
                    :
                    <time className="f6 mb2 dib tracked">
                        <small>{moment(lastTransmission).fromNow()}</small>
                    </time>
            }

            <p className="lh-copy measure center f6 black-70 tracked">
                <small>{`${totalReadingsForDevice} readings`}</small>
            </p>
        </article>
        </div>

)
};
export default DeviceCard;