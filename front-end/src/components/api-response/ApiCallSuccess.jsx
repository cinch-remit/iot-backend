import React from "react";

const ApiCallSuccess = ({ mainMessage, subtitleMessage,children }) => {
    return (
        <div className={'tc'}>
            <main className="pa4 black-80">
                <div className="measure center">
                    <header className="tc ph4">
                        <h1 className="f3 f2-m f1-l fw2 black-90 mv3 green">
                            {mainMessage}
                        </h1>
                        <h2 className="f5 f4-m f3-l fw2 black-50 mt0 lh-copy black-50">
                            {subtitleMessage}
                        </h2>
                    </header>
                    {children}
                </div>
            </main>
        </div>
    )
}

export default ApiCallSuccess;