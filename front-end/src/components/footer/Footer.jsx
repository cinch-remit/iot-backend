import React from "react";
import moment from "moment";
import {useSelector} from "react-redux";

const Footer = () => {
    const { buildInfo } = useSelector(state => state.appInfo);

    return (
        <footer className="pv4 ph3 ph5-m ph6-l mid-gray">
            <small className="f6 db tc">{`© ${moment().year()}`} <b className="ttu">iot-backend server</b>., All Rights Reserved</small>
            <div className="tc mt3">
                <a href="mailto:john@doe.com" title="Contact Us"
                   className="f6 dib ph2 link mid-gray dim">Contact Us</a>
            </div>
            {
                buildInfo.build ?
                    <div className="tc mt3">
                        <span className="fw3 f6 gray tracked">{`${buildInfo.buildId}`}</span>
                        <span className="fw3 f6 blue">{` | `}</span>
                        <span className="fw3 f6 gray tracked">{` ${buildInfo.version}`}</span>
                        <span className="fw3 f6 blue">{` | `}</span>
                        <span className="fw3 f6 gray tracked ttu">{` ${buildInfo.build}`}</span>
                    </div>
                    :
                    null
            }
        </footer>
    )
}
export default Footer;