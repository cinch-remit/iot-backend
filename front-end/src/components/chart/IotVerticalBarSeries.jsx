import React from "react";
import {XYPlot, XAxis, YAxis, HorizontalGridLines, VerticalBarSeries } from 'react-vis';

const IotVerticalBarSeries = ({ data, xTitle, yTitle, xType, yType, xMetric, yMetric }) => {

    return (<div>
        <XYPlot
            xType={xType}
            yType={yType}
            width={700}
            height={700}>
            <HorizontalGridLines />
            <VerticalBarSeries
                data={data}/>
            <XAxis title={xTitle}/>
            <YAxis title={yTitle}/>
        </XYPlot>

        <div>
            <div>
                <small className={'tracked blue'}>
                    x: {xMetric}
                </small>
            </div>


            <div className={'mt2'}>
                <small className={'tracked red'}>
                    y: {yMetric}
                </small>
            </div>

        </div>
    </div>)
};
export default IotVerticalBarSeries;