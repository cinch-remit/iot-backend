import React from "react";
import "react-vis/dist/style.css"

const ChartContainer = ({ children }) => (
    <div style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center'
    }}>
        {children}
    </div>
);

export default ChartContainer;