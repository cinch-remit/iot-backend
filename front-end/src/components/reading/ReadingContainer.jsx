import React, {Fragment} from "react";
import {Chip} from "@material-ui/core";

const ReadingContainer = ({ readings, selectedKey, handleKeySelect }) => {
    const data = readings.map((key, i) => <Chip clickable color={'secondary'}
                                                variant={selectedKey === key ? "default" : "outlined"}
                                                className={'ma1 tracked'} key={i}
                                                onClick={() => handleKeySelect(key)}
                                                label={key}/>)

    return (
        <Fragment>
            {data}
        </Fragment>

    )
}
export default ReadingContainer;