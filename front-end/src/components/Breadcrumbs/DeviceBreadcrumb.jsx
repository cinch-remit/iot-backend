import React, {useState} from 'react';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import {Redirect} from "react-router-dom";

const DeviceBreadcrumb = ({ links, currentLocation }) => {

    const [selectedLink, setSelectedLink] = useState('');
    const [linkSelected, setLinkSelected] = useState(false);

    const handleClick = link => {
        setSelectedLink(link);
        setLinkSelected(true);
    }

    if (linkSelected) {
        return <Redirect to={selectedLink}/>
    }

    return (
        <div className={'tracked fw3 f6 ml3'}>
            <Breadcrumbs aria-label="breadcrumb" separator={'›'}>
                {
                    links.map((link, i) => <Link key={i} color={link.link === currentLocation ? 'textPrimary' : 'inherit'}  onClick={() => {
                        if (link.link !== currentLocation) {
                            handleClick(link)
                        }
                    }}>
                        <small className={`tracked ${link.link === currentLocation ? 'blue' : 'black-50'} pointer`}>{link.display}</small>
                    </Link>)
                }
            </Breadcrumbs>
        </div>

    );
}

export default DeviceBreadcrumb;