import React from "react";
import HeaderMargin from "../header-margin/header-margin";
import Loading from "./Loading";

const LoadingForPage = ({ message }) => (
    <div className={'mt7 tc'}>
        <Loading>
            <p className={'ttu'}>{message}</p>
        </Loading>

        <HeaderMargin marginWithUnit={'40rem'}/>
    </div>
);

export default LoadingForPage;