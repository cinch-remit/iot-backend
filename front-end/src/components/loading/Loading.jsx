import React from "react";
import {CircularProgress} from "@material-ui/core";

const Loading = ({ children }) => (
    <div className={'tc'}>
        <CircularProgress/>
        {children}
    </div>
)

export default Loading;