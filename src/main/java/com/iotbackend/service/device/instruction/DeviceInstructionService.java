package com.iotbackend.service.device.instruction;

import com.iotbackend.api.context.DeviceInstructionContext;
import com.iotbackend.api.dto.DeviceInstructionDTO;
import com.iotbackend.domain.User;
import com.iotbackend.domain.device.Device;
import com.iotbackend.domain.device.instruction.DeviceInstruction;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Robinson Mgbah
 * Date: 2021-07-19
 * Time: 21:08
 */
public interface DeviceInstructionService {
    DeviceInstructionDTO createDeviceInstruction(DeviceInstructionContext deviceInstructionContext, String ownerEmail);
    void deleteDeviceInstruction(String deviceInstructionId, String ownerEmail, String deviceId);
    List<DeviceInstructionDTO> getAllDeviceInstructions(String ownerEmail);
    List<DeviceInstruction> getAllDeviceInstructionsForObject(String ownerEmail);
    DeviceInstructionDTO getDeviceInstructionById(String deviceInstructionId, String ownerEmail, String deviceId);
    DeviceInstruction getDeviceInstructionByIdForObject(String deviceInstructionId, String ownerEmail, String deviceId);
    void notifyUser(User user, DeviceInstruction deviceInstruction, LocalDateTime timeAdded);
    void notifyUser(User user, DeviceInstruction deviceInstruction, LocalDateTime timeAdded, String triggerValue);
    boolean performInstruction(Device device, String value);
}
