package com.iotbackend.service.device.instruction;

import com.iotbackend.api.context.DeviceInstructionContext;
import com.iotbackend.api.dto.DeviceInstructionDTO;
import com.iotbackend.dao.DeviceInstructionRepository;
import com.iotbackend.domain.User;
import com.iotbackend.domain.device.Device;
import com.iotbackend.domain.device.instruction.DeviceInstruction;
import com.iotbackend.domain.device.instruction.InstructionOperator;
import com.iotbackend.domain.device.instruction.InstructionStatus;
import com.iotbackend.domain.device.instruction.InstructionType;
import com.iotbackend.exceptions.DeviceException;
import com.iotbackend.exceptions.DeviceInstructionException;
import com.iotbackend.service.email.EmailService;
import com.iotbackend.service.email.template.EmailTemplate;
import com.iotbackend.service.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

/**
 * @author Robinson Mgbah
 * Date: 2021-07-19
 * Time: 21:14
 */
@Slf4j
@Service
public class DeviceInstructionServiceImpl implements DeviceInstructionService {

    private final UserService userService;
    private final DeviceInstructionRepository deviceInstructionRepository;
    private final EmailService emailService;

    public DeviceInstructionServiceImpl(UserService userService, DeviceInstructionRepository deviceInstructionRepository,
                                        EmailService emailService) {
        this.userService = userService;
        this.deviceInstructionRepository = deviceInstructionRepository;
        this.emailService = emailService;
    }

    /**
     * @param deviceInstructionContext A context representing instruction.
     * @param ownerEmail               the owner of the device
     * @return A DTO representation of instruction.
     */
    @Override
    public DeviceInstructionDTO createDeviceInstruction(DeviceInstructionContext deviceInstructionContext, String ownerEmail) {
        var user = userService.getUserForObject(ownerEmail);
        var device = validateDeviceForInstruction(user, deviceInstructionContext.getDeviceId());

        var deviceInstruction = DeviceInstruction.builder()
                .instructionId(KeyGenerators.string().generateKey())
                .instructionType(InstructionType.MAIL)
                .instructionOperator(InstructionOperator.of(deviceInstructionContext.getOperator()))
                .expireAt(LocalDateTime.parse(deviceInstructionContext.getExpireTime()))
                .startAt(LocalDateTime.parse(deviceInstructionContext.getStartTime()))
                .triggerValue(deviceInstructionContext.getTriggerValue())
                .build();

        deviceInstruction.setDevice(device);
        deviceInstruction.setMessage(deviceInstructionContext.getMessage());
        deviceInstruction.setInstructionStatus(InstructionStatus.ACTIVE);

        if (device.getInstructions().contains(deviceInstruction)) {
            throw new DeviceInstructionException("An instruction with same configuration for device: " + deviceInstructionContext + " already exists");
        }

        deviceInstruction = deviceInstructionRepository.save(deviceInstruction);
        log.info("instruction: {} created", deviceInstruction);
        return new DeviceInstructionDTO(deviceInstruction);
    }

    /**
     * @param deviceInstructionId Intruction ID
     * @param ownerEmail          owner of device
     * @param deviceId            ID of device
     */
    @Override
    public void deleteDeviceInstruction(String deviceInstructionId, String ownerEmail, String deviceId) {
        var user = userService.getUserForObject(ownerEmail);
        var device = validateDeviceForInstruction(user, deviceId);

        device.getInstructions()
                .stream()
                .filter(instruction -> instruction.getInstructionId().equals(deviceInstructionId))
                .findFirst()
                .ifPresent(instruction -> {
                    deviceInstructionRepository.delete(instruction);
                    log.info("Deleted instruction: {} for user: {} @ {}", deviceInstructionId, user, LocalDateTime.now());
                });
    }

    /**
     * @param ownerEmail owner of the device
     * @return A list of instructions
     */
    @Override
    public List<DeviceInstructionDTO> getAllDeviceInstructions(String ownerEmail) {
        return getAllDeviceInstructionsForObject(ownerEmail)
                .stream()
                .map(DeviceInstructionDTO::new)
                .toList();
    }

    /**
     * @param ownerEmail owner of email
     * @return A list of DeviceInstructionEntity
     */
    @Override
    public List<DeviceInstruction> getAllDeviceInstructionsForObject(String ownerEmail) {
        var user = userService.getUserForObject(ownerEmail);

        return user.getDevices()
                .stream()
                .map(Device::getInstructions)
                .flatMap(Collection::stream)
                .toList();
    }

    /**
     * @param deviceInstructionId Instruction id
     * @param ownerEmail          owner of device
     * @param deviceId            device id
     * @return A device instruction DTO
     */
    @Override
    public DeviceInstructionDTO getDeviceInstructionById(String deviceInstructionId, String ownerEmail, String deviceId) {
        var instruction = getDeviceInstructionByIdForObject(deviceInstructionId, ownerEmail, deviceId);
        return new DeviceInstructionDTO(instruction);

    }

    /**
     * @param deviceInstructionId Instruction id
     * @param ownerEmail          owner of device
     * @param deviceId            device id
     * @return A device instruction entity
     */
    @Override
    public DeviceInstruction getDeviceInstructionByIdForObject(String deviceInstructionId, String ownerEmail, String deviceId) {
        var user = userService.getUserForObject(ownerEmail);
        var device = validateDeviceForInstruction(user, deviceId);
        return device.getInstructions()
                .stream()
                .filter(instruction -> instruction.getInstructionId().equals(deviceInstructionId))
                .findFirst()
                .orElseThrow(() -> new DeviceInstructionException("Could not find instruction: " + deviceInstructionId));
    }

    /**
     * @param user              the user to be notified
     * @param deviceInstruction the instruction to be notified about
     * @param timeAdded         date time instruction was added
     */
    @Override
    public void notifyUser(User user, DeviceInstruction deviceInstruction, LocalDateTime timeAdded) {
        var email = EmailTemplate.instructionNotificationMail(deviceInstruction, user.getEmail());
        emailService.sendEmail(email);
    }

    @Override
    public void notifyUser(User user, DeviceInstruction deviceInstruction, LocalDateTime timeAdded, String triggerValue) {
        var email = EmailTemplate.instructionNotificationMailWithTemperatureData(deviceInstruction, user.getEmail(), triggerValue, timeAdded);
        emailService.sendEmail(email);
    }

    @Override
    public boolean performInstruction(Device device, String value) {
        var instructions = device.getInstructions().stream()
                .filter(deviceInstruction -> deviceInstruction.getInstructionStatus().equals(InstructionStatus.ACTIVE))
                .filter(instruction -> instruction.getStartAt().isBefore(instruction.getExpireAt()));

        instructions.forEach(deviceInstruction -> {
            var operator = deviceInstruction.getInstructionOperator();
            if (processByOperator(operator, value, deviceInstruction)) {
                notifyUser(device.getUser(), deviceInstruction, LocalDateTime.now(), value);
            }
        });

        return false;
    }

    private boolean processByOperator(InstructionOperator operator, String value, DeviceInstruction instruction) {
        switch (operator) {
            case EQUAL:
                return StringUtils.equals(value, instruction.getTriggerValue());
            case LESS_THAN:
                try {
                    var intValue = Integer.parseInt(value);
                    var triggerIntValue = Integer.parseInt(instruction.getTriggerValue());
                    return intValue < triggerIntValue;
                } catch (Exception e) {
                    return false;
                }
            case GREATER_THAN:
                try {
                    var intValue = Integer.parseInt(value);
                    var triggerIntValue = Integer.parseInt(instruction.getTriggerValue());
                    return intValue > triggerIntValue;
                } catch (Exception e) {
                    return false;
                }
            case LESS_THAN_OR_EQUAL:
                try {
                    var intValue = Integer.parseInt(value);
                    var triggerIntValue = Integer.parseInt(instruction.getTriggerValue());
                    return intValue <= triggerIntValue;
                } catch (Exception e) {
                    return false;
                }
            case GREATER_THAN_OR_EQUAL:
                try {
                    var intValue = Integer.parseInt(value);
                    var triggerIntValue = Integer.parseInt(instruction.getTriggerValue());
                    return intValue >= triggerIntValue;
                } catch (Exception e) {
                    return false;
                }

            default:
                return false;
        }
    }

    /**
     * @param user     the user for which instruction should be validated
     * @param deviceId device id
     * @return A device entity
     */
    public Device validateDeviceForInstruction(User user, String deviceId) {
        if (!user.getDevices().contains(Device.builder()
                .deviceId(deviceId)
                .build())) {
            throw new DeviceInstructionException("Device " + deviceId + " does not exist");
        }

        return user.getDevices().stream()
                .filter(dev -> dev.getDeviceId().equals(deviceId))
                .findFirst()
                .orElseThrow(() -> new DeviceException("Could not find device " + deviceId));
    }
}
