package com.iotbackend.service.device;

import com.iotbackend.api.dto.DeviceDTO;
import com.iotbackend.domain.device.Device;
import com.iotbackend.domain.User;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 20:42
 */
public interface DeviceService {
    DeviceDTO registerDevice(String email, String deviceId);
    List<DeviceDTO> getAllDevicesForUser(String email);
    DeviceDTO getDeviceByIdForDTO(String email, String deviceId);
    Device getDeviceByIdForObject(String email, String deviceId);
    void notifyUser(User user, Device device, LocalDateTime timeAdded);
    void addDeviceName(String email, String deviceId, String deviceName);
}
