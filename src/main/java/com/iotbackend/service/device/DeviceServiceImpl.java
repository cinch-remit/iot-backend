package com.iotbackend.service.device;

import com.iotbackend.api.dto.DeviceDTO;
import com.iotbackend.api.dto.DeviceDataDTO;
import com.iotbackend.api.dto.DeviceInstructionDTO;
import com.iotbackend.api.dto.ReadingDTO;
import com.iotbackend.dao.DeviceRepository;
import com.iotbackend.domain.Reading;
import com.iotbackend.domain.User;
import com.iotbackend.domain.device.Device;
import com.iotbackend.domain.notification.Notification;
import com.iotbackend.domain.notification.NotificationType;
import com.iotbackend.exceptions.DeviceException;
import com.iotbackend.service.notification.NotificationService;
import com.iotbackend.service.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 21:04
 */
@Slf4j
@Service
public class DeviceServiceImpl implements DeviceService {
    private final UserService userService;
    private final DeviceRepository repository;
    private final NotificationService notificationService;

    public DeviceServiceImpl(UserService userService, DeviceRepository repository, NotificationService notificationService) {
        this.userService = userService;
        this.repository = repository;
        this.notificationService = notificationService;
    }

    @Override
    public DeviceDTO registerDevice(String email, String deviceId) {
        var user = userService.getUserForObject(email);
        var currentDevices = user.getDevices();

        var addedOrExistingDevice = currentDevices
                .stream()
                .filter(device -> device.getDeviceId().equals(deviceId))
                .findFirst()
                .orElseGet(() -> {
                    log.info("Found no existing device: {}, adding a new device", deviceId);
                    var newDevice = Device.builder()
                            .deviceId(deviceId)
                            .build();
                    newDevice.setUser(user);
                    var saved = repository.save(newDevice);
                    notifyUser(user,newDevice, LocalDateTime.now());
                    return saved;
                });

        return new DeviceDTO(addedOrExistingDevice);
    }

    @Override
    public List<DeviceDTO> getAllDevicesForUser(String email) {
        var user = userService.getUserForObject(email);
        return user.getDevices()
                .stream()
                .map(device -> {
                    var readings = device.getReadings();
                    var dataLayers = mapDataIntoLayers(readings);
                    var dto = new DeviceDataDTO(device, dataLayers, readings.size());
                    var readingsDTO = readings.stream()
                            .sorted()
                            .map(ReadingDTO::new)
                            .toList();
                    dto.setReadings(readingsDTO);

                    dto.setDeviceInstructions(device.getInstructions()
                            .stream()
                            .map(DeviceInstructionDTO::new)
                            .toList());
                    return dto;
                })
                .collect(Collectors.toList());
    }

    @Override
    public Device getDeviceByIdForObject(String email, String deviceId) {
        var user = userService.getUserForObject(email);
        return user.getDevices()
                .stream()
                .filter(device -> device.getDeviceId().equals(deviceId))
                .findFirst()
                .orElseThrow(() -> new DeviceException("Device not found"));
    }

    @Override
    public DeviceDTO getDeviceByIdForDTO(String email, String deviceId) {
        var device = getDeviceByIdForObject(email, deviceId);

        var readings = device.getReadings();
        var dataLayers = mapDataIntoLayers(readings);

        return new DeviceDataDTO(device, dataLayers, readings.size());
    }

    private Map<String, Map<String, List<ReadingDTO>>> mapDataIntoLayers(List<Reading> readings) {
        Map<String, Map<String, List<ReadingDTO>>> dataLayers = new HashMap<>();

        readings
                .forEach(reading -> {
            var typeOfReading = reading.getTypeOfReading();

            if (dataLayers.containsKey(typeOfReading)) {
                var data = dataLayers.get(typeOfReading);
                var metric = reading.getMetric();
                if (data.containsKey(metric)) {
                    var dataByMetric = new ArrayList<>(data.get(metric));
                    dataByMetric.add(new ReadingDTO(reading));

                    data.put(metric, dataByMetric);
                }else {
                    data.put(metric, Collections.singletonList(new ReadingDTO(reading)));
                }
            }else {
                Map<String, List<ReadingDTO>> layer = new HashMap<>();
                layer.put(reading.getMetric(), Collections.singletonList(new ReadingDTO(reading)));

                dataLayers.put(reading.getTypeOfReading(), layer);
            }
        });

        return dataLayers;
    }

    @Override
    public void notifyUser(User user, Device device, LocalDateTime timeAdded) {
        Map<String, String> notificationDetails = new HashMap<>();

        notificationDetails.put("Device Id", device.getDeviceId());
        notificationDetails.put("Time Added", timeAdded.toString());

        var deviceNotification = Notification.builder()
                .notificationDetails(notificationDetails)
                .user(user)
                .type(NotificationType.DEVICE_ADDED)
                .notificationId(KeyGenerators.string().generateKey())
                .build();

        notificationService.saveAndSend(deviceNotification);
    }

    @Override
    public void addDeviceName(String email, String deviceId, String deviceName) {
        log.info("changing device name to {}", deviceName);
        var device = getDeviceByIdForObject(email, deviceId);
        device.setDeviceName(deviceName);

        repository.save(device);
    }
}
