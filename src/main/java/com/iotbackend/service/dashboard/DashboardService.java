package com.iotbackend.service.dashboard;

import com.iotbackend.api.context.DashboardContext;
import com.iotbackend.api.dto.DashboardDTO;
import com.iotbackend.domain.User;
import com.iotbackend.domain.dashboard.Dashboard;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-30
 * Time: 08:27
 */
public interface DashboardService {
    DashboardDTO createDashboard(DashboardContext dashboardContext, String ownerEmail);
    void updateDashboard(DashboardContext dashboardContext,String dashboardName, String ownerEmail);
    void deleteDashboard(String dashboardName, String ownerEmail);
    List<DashboardDTO> getAllDashboards(String ownerEmail);
    List<Dashboard> getAllDashboardsForObject(String ownerEmail);
    DashboardDTO getDashboardByName(String dashboardName, String ownerEmail);
    Dashboard getDashboardByNameForObject(String dashboardName, String ownerEmail);
    void notifyUser(User user, Dashboard dashboard, LocalDateTime timeAdded);
}
