package com.iotbackend.service.dashboard;

import com.iotbackend.api.context.DashboardContext;
import com.iotbackend.api.dto.DashboardDTO;
import com.iotbackend.dao.DashboardRepository;
import com.iotbackend.domain.User;
import com.iotbackend.domain.dashboard.Dashboard;
import com.iotbackend.domain.device.Device;
import com.iotbackend.domain.notification.Notification;
import com.iotbackend.domain.notification.NotificationType;
import com.iotbackend.exceptions.DashboardException;
import com.iotbackend.service.notification.NotificationService;
import com.iotbackend.service.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-30
 * Time: 08:32
 */
@Service
@Slf4j
public class DashboardServiceImpl implements DashboardService {
    private final UserService userService;
    private final DashboardRepository dashboardRepository;
    private final NotificationService notificationService;

    public DashboardServiceImpl(UserService userService, DashboardRepository dashboardRepository, NotificationService notificationService) {
        this.userService = userService;
        this.dashboardRepository = dashboardRepository;
        this.notificationService = notificationService;
    }

    /**
     * @param dashboardContext A representation of dashboard request.
     * @param ownerEmail       The email of the user who owns this dashboard(the authenticated user)
     * @return returns a DTO object for the newly created dashboard.
     * @implNote Creates a new dashboard if dashboard doesn't currently exist(Uses name to check equality of dashboards.).
     */
    @Override
    public DashboardDTO createDashboard(DashboardContext dashboardContext, String ownerEmail) {
        var user = userService.getUserForObject(ownerEmail);

        if (!user.getDevices().contains(Device.builder()
                .deviceId(dashboardContext.getDeviceId())
                .build())) {
            throw new DashboardException("Device " + dashboardContext.getDeviceId() + " does not exist");
        }

        var userDashboards = user.getDashboards();
        if (userDashboards.contains(Dashboard.builder()
                .name(dashboardContext.getName())
                .build())) {
            throw new DashboardException("A dashboard with name: " + dashboardContext.getName() + " already exists");
        }

        var dashboard = Dashboard.builder()
                .deviceId(dashboardContext.getDeviceId())
                .name(dashboardContext.getName())
                .metric(dashboardContext.getMetric())
                .typeOfReading(dashboardContext.getTypeOfReading())
                .xAxis(dashboardContext.getXAxis())
                .yAxis(dashboardContext.getYAxis())
                .build();

        dashboard.setUser(user);

        dashboard = dashboardRepository.save(dashboard);
        log.info("dashboard: {} created", dashboard);

        notifyUser(user, dashboard, LocalDateTime.now());
        return new DashboardDTO(dashboard);
    }

    /**
     * @param dashboardContext dashboardContext  A representation of dashboard request.
     * @param dashboardName    The name of the dashboard intended to be updated.
     * @param ownerEmail       ownerEmail The email of the user who owns this dashboard(the authenticated user)
     * @implNote Note that if the new name matches an existing name for a dashboard currently owned by user, an exception is thrown.
     */
    @Override
    public void updateDashboard(DashboardContext dashboardContext, String dashboardName, String ownerEmail) {
        var userDashboards = getAllDashboardsForObject(ownerEmail);
        var dashboardToBeUpdated = userDashboards
                .stream()
                .filter(dashboard -> dashboard.getName().equals(dashboardName))
                .findFirst()
                .orElseThrow(() -> new DashboardException("dashboard: " + dashboardName + " does not exist."));

        var update = false;

        if (StringUtils.isNotEmpty(dashboardContext.getName()) && !StringUtils.equals(dashboardContext.getName(), dashboardToBeUpdated.getName())) {
            if (userDashboards.contains(Dashboard.builder()
                    .name(dashboardContext.getName())
                    .build())) {
                throw new DashboardException("A dashboard with new name already exists");
            }
            log.info("Updated name from {} to {} for {}", dashboardToBeUpdated.getName(), dashboardContext.getName(), dashboardToBeUpdated);
            dashboardToBeUpdated.setName(dashboardContext.getName());
            update = true;
        }

        if (StringUtils.isNotEmpty(dashboardContext.getMetric()) && !StringUtils.equals(dashboardContext.getMetric(), dashboardToBeUpdated.getMetric())) {
            log.info("Updated metric from {} to {} for {}", dashboardToBeUpdated.getMetric(), dashboardContext.getMetric(), dashboardToBeUpdated);
            dashboardToBeUpdated.setMetric(dashboardContext.getMetric());
            update = true;
        }

        if (StringUtils.isNotEmpty(dashboardContext.getDeviceId()) && !StringUtils.equals(dashboardContext.getDeviceId(), dashboardToBeUpdated.getDeviceId())) {
            log.info("Updated device from {} to {} for {}", dashboardToBeUpdated.getDeviceId(), dashboardContext.getDeviceId(), dashboardToBeUpdated);
            dashboardToBeUpdated.setDeviceId(dashboardContext.getDeviceId());
            update = true;
        }

        if (StringUtils.isNotEmpty(dashboardContext.getTypeOfReading()) && !StringUtils.equals(dashboardContext.getTypeOfReading(), dashboardToBeUpdated.getTypeOfReading())) {
            log.info("Updated typeOfReading from {} to {} for {}", dashboardToBeUpdated.getTypeOfReading(), dashboardContext.getTypeOfReading(), dashboardToBeUpdated);
            dashboardToBeUpdated.setTypeOfReading(dashboardContext.getTypeOfReading());
            update = true;
        }

        if (StringUtils.isNotEmpty(dashboardContext.getXAxis()) && !StringUtils.equals(dashboardContext.getXAxis(), dashboardToBeUpdated.getXAxis())) {
            log.info("Updated xAxis from {} to {} for {}", dashboardToBeUpdated.getXAxis(), dashboardContext.getXAxis(), dashboardToBeUpdated);
            dashboardToBeUpdated.setXAxis(dashboardContext.getXAxis());
            update = true;
        }

        if (StringUtils.isNotEmpty(dashboardContext.getYAxis()) && !StringUtils.equals(dashboardContext.getYAxis(), dashboardToBeUpdated.getYAxis())) {
            log.info("Updated yAxis from {} to {} for {}", dashboardToBeUpdated.getYAxis(), dashboardContext.getYAxis(), dashboardToBeUpdated);
            dashboardToBeUpdated.setYAxis(dashboardContext.getYAxis());
            update = true;
        }

        if (update) {
            dashboardRepository.save(dashboardToBeUpdated);
            log.info("updated and saved dashboard: {}", dashboardToBeUpdated);
        }
    }

    /**
     * @param dashboardName The name of the dashboard to be deleted
     * @param ownerEmail    The email of the user who owns this dashboard(the authenticated user)
     */
    @Override
    public void deleteDashboard(String dashboardName, String ownerEmail) {
        var user = userService.getUserForObject(ownerEmail);
        dashboardRepository.findDashboardByNameAndUser(dashboardName, user)
                .ifPresent(dashboard -> {
                    dashboardRepository.delete(dashboard);
                    log.info("deleted dashboard: {} for user:{} @ {}", dashboardName, user.getEmail(), LocalDateTime.now());
                });
    }

    /**
     * @param ownerEmail The email of the user who owns this dashboard(the authenticated user)
     * @return a list of all dashboard entities owned by the user.
     */
    @Override
    public List<Dashboard> getAllDashboardsForObject(String ownerEmail) {
        var user = userService.getUserForObject(ownerEmail);
        return user.getDashboards();
    }

    /**
     * @param ownerEmail The email of the user who owns this dashboard(the authenticated user)
     * @return a dto list of dashboard entities owned by the user.
     */
    @Override
    public List<DashboardDTO> getAllDashboards(String ownerEmail) {
        return getAllDashboardsForObject(ownerEmail)
                .stream()
                .map(DashboardDTO::new)
                .collect(Collectors.toList());
    }

    /**
     * @param dashboardName the name of the intended dashboard to retriece
     * @param ownerEmail    The email of the user who owns this dashboard(the authenticated user)
     * @return a database entity of the retrieved database
     */
    @Override
    public Dashboard getDashboardByNameForObject(String dashboardName, String ownerEmail) {
        var user = userService.getUserForObject(ownerEmail);
        return user.getDashboards()
                .stream()
                .filter(dashboard -> dashboard.getName().equals(dashboardName))
                .findFirst()
                .orElseThrow();
    }

    /**
     * @param dashboardName the name of the intended dashboard to retrieve
     * @param ownerEmail    The email of the user who owns this dashboard(the authenticated user)
     * @return a dto object of the retrieved dashboard.
     */
    @Override
    public DashboardDTO getDashboardByName(String dashboardName, String ownerEmail) {
        return new DashboardDTO(getDashboardByNameForObject(dashboardName, ownerEmail));
    }

    @Override
    public void notifyUser(User user, Dashboard dashboard, LocalDateTime timeAdded) {
        Map<String, String> notificationDetails = new HashMap<>();

        notificationDetails.put("Dashboard Name", dashboard.getName());
        notificationDetails.put("Time Added", timeAdded.toString());

        var dashboardNotification = Notification.builder()
                .notificationDetails(notificationDetails)
                .user(user)
                .type(NotificationType.DASHBOARD_ADDED)
                .notificationId(KeyGenerators.string().generateKey())
                .build();

        notificationService.saveAndSend(dashboardNotification);
    }
}
