package com.iotbackend.service.code;

import com.iotbackend.dao.CodeRepository;
import com.iotbackend.domain.codes.Code;
import com.iotbackend.exceptions.CodeException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-18
 * Time: 18:06
 */
@Slf4j
@Service
public class CodeServiceImpl implements CodeService {

    private final CodeRepository repository;

    public CodeServiceImpl(CodeRepository repository) {
        this.repository = repository;
    }

    @Override
    public Code useOneTimeCode(String oneTimeCode) {
        var code = findRequestCodeByOneTimeCode(oneTimeCode);
        code.setUsed(true);
        code.setUsedAt(LocalDateTime.now());

        log.info("Code {} has been used @ {}", code, LocalDateTime.now());
        return repository.save(code);
    }

    @Override
    public Code findRequestCodeByOneTimeCode(String oneTimeCode) {
        return repository.findByOneTimeCode(oneTimeCode)
                .map(code -> {
                    validateCode(code);
                    return code;
                })
                .orElseThrow(() -> new CodeException("Could not find code"));
    }

    @Override
    public Code save(Code code) {
        return repository.save(code);
    }

    @Override
    public void validateCode(Code code) {
        if (code.isUsed()) {
            log.error("request code:{} has already been used", code.getOneTimeCode());
            throw new CodeException("Code has already been used");
        } else if (code.isExpired()) {
            log.error("code:{} has already been used", code);
            throw new CodeException("Code has expired");
        }
    }
}
