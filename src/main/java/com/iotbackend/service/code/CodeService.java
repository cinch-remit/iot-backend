package com.iotbackend.service.code;

import com.iotbackend.domain.codes.Code;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-18
 * Time: 18:05
 */
public interface CodeService {
    Code useOneTimeCode(String oneTimeCode);

    Code findRequestCodeByOneTimeCode(String oneTimeCode);

    Code save(Code code);

    void validateCode(Code code);
}
