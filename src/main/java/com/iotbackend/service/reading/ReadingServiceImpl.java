package com.iotbackend.service.reading;

import com.iotbackend.api.context.ReadingRegisterContext;
import com.iotbackend.api.dto.ConnectedReadingDTO;
import com.iotbackend.api.dto.DeviceDTO;
import com.iotbackend.api.dto.ReadingDTO;
import com.iotbackend.dao.ReadingRepository;
import com.iotbackend.domain.Reading;
import com.iotbackend.domain.device.Device;
import com.iotbackend.domain.notification.Notification;
import com.iotbackend.domain.notification.NotificationType;
import com.iotbackend.exceptions.ReadingException;
import com.iotbackend.service.device.DeviceService;
import com.iotbackend.service.device.instruction.DeviceInstructionService;
import com.iotbackend.service.notification.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 21:15
 */
@Slf4j
@Service
public class ReadingServiceImpl implements ReadingService {
    private final DeviceService deviceService;
    private final ReadingRepository repository;
    private final NotificationService notificationService;
    private final DeviceInstructionService deviceInstructionService;

    public ReadingServiceImpl(DeviceService deviceService, ReadingRepository repository, NotificationService notificationService,
                              DeviceInstructionService deviceInstructionService) {
        this.deviceService = deviceService;
        this.repository = repository;
        this.notificationService = notificationService;
        this.deviceInstructionService = deviceInstructionService;
    }

    @Override
    public boolean registerReadings(String email, String deviceId, ReadingRegisterContext readingRegisterContext, LocalDateTime timeOfReading) {
        var device = deviceService.getDeviceByIdForObject(email, deviceId);
        var reading = Reading.builder()
                .timeOfReading(timeOfReading)
                .typeOfReading(readingRegisterContext.getTypeOfReading())
                .value(readingRegisterContext.getValue())
                .metric(readingRegisterContext.getMetric())
                .build();

        device.setLastTransmission(timeOfReading);
        reading.setDevice(device);
        var saved = repository.saveAndFlush(reading);
        notifyUser(device, saved, LocalDateTime.now());
        deviceInstructionService.performInstruction(device, reading.getValue());

        return true;
    }

    private Reading registerConnectedReading(String email, String deviceId, ReadingRegisterContext readingRegisterContext, LocalDateTime timeOfReading, String connectionId) {
        var device = deviceService.getDeviceByIdForObject(email, deviceId);

        var reading = Reading.builder()
                .timeOfReading(timeOfReading)
                .typeOfReading(readingRegisterContext.getTypeOfReading())
                .value(readingRegisterContext.getValue())
                .metric(readingRegisterContext.getMetric())
                .build();

        reading.setConnected(true);
        reading.setConnectionId(connectionId);

        device.setLastTransmission(timeOfReading);
        reading.setDevice(device);
        var saved = repository.save(reading);
        notifyUser(device, saved, LocalDateTime.now());
        deviceInstructionService.performInstruction(device, reading.getValue());

        return saved;
    }

    @Override
    public ConnectedReadingDTO registerConnectedReadings(String email, String deviceId, List<ReadingRegisterContext> readings) {
        var timeOfReading = LocalDateTime.now();
        var connectionId = KeyGenerators.string().generateKey();

        var readingsDTO = readings
                .stream()
                .map(reading -> {
                    var saved = registerConnectedReading(email, deviceId, reading, timeOfReading, connectionId);
                    return ReadingDTO.builder()
                            .reading(saved)
                            .build();
                })
                .toList();

        return ConnectedReadingDTO.builder()
                .connectionId(connectionId)
                .readings(readingsDTO)
                .timeOfReading(timeOfReading.toString())
                .build();
    }

    @Override
    public ReadingDTO getReadingById(String email,String deviceId, Long readingId) {
        var device = deviceService.getDeviceByIdForObject(email, deviceId);
        return device.getReadings()
                .stream()
                .filter(reading -> reading.getId().equals(readingId))
                .findFirst()
                .map(ReadingDTO::new)
                .orElseThrow(() -> new ReadingException("Could not find reading with Id: "+readingId));
    }

    @Override
    public List<ReadingDTO> getAllReadingsForUser(String email) {
        var devices = deviceService.getAllDevicesForUser(email);

        return devices.stream()
                .map(DeviceDTO::getReadings)
                .flatMap(Collection::stream)
                .toList();
    }

    @Override
    public List<ReadingDTO> getAllReadingsForDevice(String email, String deviceId) {
        var device = deviceService.getDeviceByIdForDTO(email, deviceId);
        return device.getReadings();
    }

    @Override
    public List<ReadingDTO> getAllReadingsByType(String email, String type) {
        var readings = getAllReadingsForUser(email);
        return readings
                .stream()
                .filter(readingDTO -> readingDTO.getTypeOfReading().equals(type))
                .toList();
    }

    @Override
    public void notifyUser(Device device, Reading reading, LocalDateTime timeAdded) {
        Map<String, String> notificationDetails = new HashMap<>();

        notificationDetails.put("Time Added", timeAdded.toString());
        notificationDetails.put("Device Id", device.getDeviceId());
        notificationDetails.put("Reading Id", reading.getId().toString());

        var readingNotification = Notification.builder()
                .notificationDetails(notificationDetails)
                .user(device.getUser())
                .type(NotificationType.READING_ADDED)
                .notificationId(KeyGenerators.string().generateKey())
                .build();

        notificationService.saveAndSend(readingNotification);
    }

    @Override
    public Map<String, ConnectedReadingDTO> getConnectedReadings(String email, String deviceId) {
        var device = deviceService.getDeviceByIdForObject(email, deviceId);
        var connectedReadings = repository.findAllByDeviceAndConnectedTrue(device);

        return mapDataIntoLayers(connectedReadings);
    }

    private Map<String, ConnectedReadingDTO> mapDataIntoLayers(List<Reading> readings) {
        Map<String, ConnectedReadingDTO> dataLayers = new HashMap<>();

        readings
                .forEach(reading -> {
                    var connectionId = reading.getConnectionId();
                    ConnectedReadingDTO connectionDTO;
                    if (dataLayers.containsKey(connectionId)) {
                        connectionDTO = dataLayers.get(connectionId);
                        var updatedReadingsDTOList = new ArrayList<>(connectionDTO.getReadings());
                        updatedReadingsDTOList.add(ReadingDTO.builder()
                                        .reading(reading)
                                .build());
                        connectionDTO.setReadings(updatedReadingsDTOList);
                    }else {
                        connectionDTO = ConnectedReadingDTO.builder()
                                .timeOfReading(reading.getTimeOfReading().toString())
                                .connectionId(reading.getConnectionId())
                                .readings(List.of(ReadingDTO.builder()
                                        .reading(reading)
                                        .build()))
                                .build();
                    }
                    dataLayers.put(connectionId, connectionDTO);
                });

        return dataLayers;
    }
}
