package com.iotbackend.service.reading;

import com.iotbackend.api.context.ReadingRegisterContext;
import com.iotbackend.api.dto.ConnectedReadingDTO;
import com.iotbackend.api.dto.ReadingDTO;
import com.iotbackend.domain.Reading;
import com.iotbackend.domain.device.Device;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 20:44
 */
public interface ReadingService {
    boolean registerReadings(String email, String deviceId, ReadingRegisterContext readingRegisterContext, LocalDateTime timeOfReading);
    ConnectedReadingDTO registerConnectedReadings(String email, String deviceId, List<ReadingRegisterContext> readings);
    ReadingDTO getReadingById(String email,String deviceId, Long readingId);
    List<ReadingDTO> getAllReadingsForUser(String email);
    List<ReadingDTO> getAllReadingsForDevice(String email, String deviceId);
    List<ReadingDTO> getAllReadingsByType(String email, String type);
    void notifyUser(Device device, Reading reading, LocalDateTime timeAdded);
    Map<String, ConnectedReadingDTO> getConnectedReadings(String email, String deviceId);
}
