package com.iotbackend.service.email.template;

import com.iotbackend.domain.device.instruction.DeviceInstruction;
import com.iotbackend.domain.email.Email;
import com.iotbackend.domain.email.EmailType;

import java.time.LocalDateTime;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-18
 * Time: 17:34
 */
public class EmailTemplate {

    private static final String KIND_REGARDS = "\nKind regards.";

    public static Email registrationEmail(String confirmationLink, String userEmail) {
        return Email.builder()
                .recipientEmail(userEmail)
                .subject("Welcome to IOT Backend Server 2021.")
                .type(EmailType.VERIFICATION)
                .mailContent(registrationMailContent(confirmationLink))
                .build();
    }

    public static Email confirmationEmail(String serverAddress, String userEmail) {
        return Email.builder()
                .recipientEmail(userEmail)
                .subject("Registration Complete.")
                .type(EmailType.CONFIRMATION)
                .mailContent(confirmationMailContent(serverAddress))
                .build();
    }

    public static Email passwordResetEmail(String serverAddress, String userEmail) {
        return Email.builder()
                .recipientEmail(userEmail)
                .subject("Password change.")
                .type(EmailType.PASSWORD_RESET)
                .mailContent(passwordResetMailContent(serverAddress))
                .build();
    }

    public static Email instructionNotificationMail(DeviceInstruction instruction, String userEmail) {
        return Email.builder()
                .recipientEmail(userEmail)
                .subject("Notification from " + instruction.getDevice().getDeviceId())
                .type(EmailType.DEVICE_INSTRUCTION)
                .mailContent(instruction.getMessage())
                .build();
    }

    public static Email instructionNotificationMailWithTemperatureData(DeviceInstruction instruction, String userEmail, String value, LocalDateTime timeAdded) {
        return Email.builder()
                .recipientEmail(userEmail)
                .subject("Notification from " + instruction.getDevice().getDeviceId())
                .type(EmailType.DEVICE_INSTRUCTION)
                .mailContent(instructionMessage(instruction, value, timeAdded))
                .build();
    }

    private static String registrationMailContent(String link) {
        return "Hi! \nThanks for registering with the IOT Backend Server.\n Click on the link below to finish your registration\n" + link + KIND_REGARDS;
    }

    private static String passwordResetMailContent(String link) {
        return "Hi! \nYou requested a password change.\n Click on the link below to change your password." +
                "\nIf you didn't make this request, please ignore this mail." + link + KIND_REGARDS;
    }

    private static String confirmationMailContent(String serverAddress) {
        return "Hi! \nRegistration complete.\nLogin at " + serverAddress + KIND_REGARDS;
    }

    private static String instructionMessage(DeviceInstruction instruction, String value, LocalDateTime dateAdded) {
        return instruction.getMessage() + "\n======================\n DETAILS\nDevice: "
                + instruction.getDevice().getDeviceId()
                + "\nTemperature: " + value
                + "\nTime: " + dateAdded;
    }
}
