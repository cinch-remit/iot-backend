package com.iotbackend.service.email;

import com.iotbackend.domain.email.Email;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-18
 * Time: 17:17
 */
public interface EmailService {
    boolean sendEmail(Email email);
    boolean sendEmailWithAttachment(Email email, String attachmentName, String fileName);
}
