package com.iotbackend.service.email;

import com.iotbackend.dao.EmailRepository;
import com.iotbackend.domain.email.Email;
import com.iotbackend.domain.email.EmailStatus;
import com.iotbackend.util.EmailUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-18
 * Time: 17:17
 */
@Slf4j
@Service
public class EmailServiceImpl implements EmailService {
    private final EmailRepository repository;
    private final JavaMailSender javaMailSender;

    @Value("${devMail}")
    private String devMail;

    @Value("${mailFrom}")
    private String mailFrom;

    public EmailServiceImpl(EmailRepository repository, JavaMailSender javaMailSender) {
        this.repository = repository;
        this.javaMailSender = javaMailSender;
    }

    @Override
    public boolean sendEmail(Email email) {
            log.info("Sending email to {}", maskEmail(email.getRecipientEmail()));
            var messagePreparator = EmailUtil.mimeMessagePreparator(email, devMail, mailFrom);
            return send(messagePreparator, email);
    }


    @Override
    public boolean sendEmailWithAttachment(Email email, String attachmentName, String fileName) {
            var messagePreparator = EmailUtil.mimeMessagePreparatorAttachment(email, attachmentName, fileName, devMail);
            return send(messagePreparator, email);
    }

    private boolean send(MimeMessagePreparator messagePreparator, Email email) {
        try {
            javaMailSender.send(messagePreparator);
            email.setStatus(EmailStatus.SENT);
            return true;
        } catch (Exception e) {
            log.error("Could not send mail..setting status to retry", e);
            email.setStatus(EmailStatus.RETRY);
        } finally {
            int sendCount = email.getSendCount() + 1;
            email.setSendCount(sendCount);
            repository.save(email);
        }
        return false;
    }

    public static String maskEmail(String email) {
        String[] data = email.split("@");
        return data[0]
                .replaceAll(".", "*")+"@"+data[1];
    }
}
