package com.iotbackend.service.notificationdispatcher;

import com.iotbackend.api.dto.NotificationDTO;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import javax.security.auth.login.AccountException;
import java.util.Map;
import java.util.Set;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-28
 * Time: 18:49
 */
public interface NotificationDispatcherService {
    void add(String sessionId, String email) throws AccountException;
    void remove(String sessionId, String iotId);
    void dispatch(NotificationDTO notification);
    void sessionDisconnectionHandler(SessionDisconnectEvent event);
    Set<String> getListeners();
    Map<String, Set<String>> getUserListeners();
}
