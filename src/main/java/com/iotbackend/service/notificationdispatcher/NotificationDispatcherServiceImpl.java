package com.iotbackend.service.notificationdispatcher;

import com.iotbackend.api.dto.NotificationDTO;
import com.iotbackend.service.user.UserService;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import javax.security.auth.login.AccountException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-28
 * Time: 18:50
 */
@Service
@Slf4j
public class NotificationDispatcherServiceImpl implements NotificationDispatcherService {

    private final SimpMessagingTemplate simpMessagingTemplate;
    private final UserService userService;

    @Getter
    private final Set<String> listeners = new HashSet<>();

    @Getter
    private final Map<String, Set<String>> userListeners = new HashMap<>();

    public NotificationDispatcherServiceImpl(SimpMessagingTemplate simpMessagingTemplate, UserService userService) {
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.userService = userService;
    }

    @Override
    public void add(String sessionId, String email) throws AccountException {
        var user = userService.getUserForObject(email);
        var iotId = user.getIotId();

        if (userListeners.containsKey(user.getIotId())) {
            Set<String> listners = this.userListeners.get(iotId);
            listners.add(sessionId);
            this.userListeners.put(iotId, listners);
        }else {
            Set<String> sessionIdsForUser = new HashSet<>();
            sessionIdsForUser.add(sessionId);
            userListeners.put(iotId, sessionIdsForUser);
        }
    }

    @Override
    public void remove(String sessionId, String iotId) {
        userListeners.computeIfPresent(iotId, (key, lists) -> {
            lists.remove(sessionId);
            return lists;
        });
    }

    @Override
    public void dispatch(NotificationDTO notification) {
        String iotId = notification.getIotId();
        Set<String> listners = userListeners.get(iotId);

        if (listners != null) {
            listners
                    .forEach(listener -> {
                        log.info("dispatching notification to:{} for {}", listener, iotId);
                        var headerAccessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE);
                        headerAccessor.setSessionId(listener);
                        headerAccessor.setLeaveMutable(true);

                        simpMessagingTemplate.convertAndSendToUser(
                                listener,
                                "/notification/" + iotId,
                                notification,
                                headerAccessor.getMessageHeaders());
                    });
        }
    }

    @EventListener
    @Override
    public void sessionDisconnectionHandler(SessionDisconnectEvent event) {
        String sessionId = event.getSessionId();
        Set<String> keys = userListeners.keySet();
        keys.parallelStream()
                .forEach(s -> {
                    Set<String> sessionIds = userListeners.get(s);
                    if (sessionIds.contains(sessionId)) {
                        try {
                            remove(sessionId, s);
                        } catch (Exception e) {
                            log.error("could not disconnect session..see error", e);
                        }
                    }
                });
    }

}
