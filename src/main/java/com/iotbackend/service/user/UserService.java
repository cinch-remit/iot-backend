package com.iotbackend.service.user;

import com.iotbackend.api.context.UserRegisterContext;
import com.iotbackend.api.dto.UserDTO;
import com.iotbackend.domain.User;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 20:38
 */
public interface UserService {
    User getUserForObject(String email);
    UserDTO getUserForDTO(String email);
    void registerUser(UserRegisterContext userRegisterContext);
    void confirmUserRegistration(String oneTimeCode);
    void requestNewRegistrationCode(String email);
    void requestPasswordChangeCode(String email);
    void resetPassword(String oneTimeCode, String encryptedPassword);
    void updateUserName(String email,String userName);
}
