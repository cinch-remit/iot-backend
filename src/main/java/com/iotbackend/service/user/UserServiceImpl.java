package com.iotbackend.service.user;

import com.iotbackend.api.context.UserRegisterContext;
import com.iotbackend.api.dto.UserDTO;
import com.iotbackend.dao.UserRepository;
import com.iotbackend.domain.User;
import com.iotbackend.domain.codes.Code;
import com.iotbackend.domain.codes.CodeType;
import com.iotbackend.exceptions.CodeException;
import com.iotbackend.exceptions.UserException;
import com.iotbackend.service.code.CodeService;
import com.iotbackend.service.email.EmailService;
import com.iotbackend.service.email.template.EmailTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Base64;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 20:49
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final EmailService emailService;
    private final CodeService codeService;

    @Value("${swagger.server-address}")
    private String serverAddress;

    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder, EmailService emailService, CodeService codeService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.emailService = emailService;
        this.codeService = codeService;
    }

    @Override
    public User getUserForObject(String email) {
        return userRepository.findUserByEmail(email)
                .orElseThrow(() -> new UserException("Could not find user"));
    }

    @Override
    public UserDTO getUserForDTO(String email) {
        var user = getUserForObject(email);
        return new UserDTO(user);
    }

    @Override
    public void registerUser(UserRegisterContext userRegisterContext) {
        var decodedPassword = new String(Base64.getDecoder().decode(userRegisterContext.getPassword()), StandardCharsets.UTF_8);

        var userOptional = userRepository.findUserByEmail(userRegisterContext.getEmail());

        if (userOptional.isPresent()) {
            throw new UserException("User already exists");
        }

        var user = User.builder()
                .roles("USER,API")
                .email(userRegisterContext.getEmail())
                .password(passwordEncoder.encode(decodedPassword))
                .iotId(KeyGenerators.string().generateKey())
                .build();

        userRepository.save(user);
        log.info("added new user: {}", user);

        var generatedCode = generateCode(user, CodeType.VERIFICATION);
        var confirmationLink = serverAddress+"/confirm/"+ generatedCode.getOneTimeCode();

        emailService.sendEmail(EmailTemplate.registrationEmail(confirmationLink, user.getEmail()));
    }

    @Override
    public void requestNewRegistrationCode(String email) {
        var user = getUserForObject(email);
        if (user.isVerified()) {
            throw new UserException("User already verified, login instead");
        }

        var generatedCode = generateCode(user, CodeType.VERIFICATION);
        var confirmationLink = serverAddress+"/confirm/"+ generatedCode.getOneTimeCode();

        emailService.sendEmail(EmailTemplate.registrationEmail(confirmationLink, user.getEmail()));
    }

    @Override
    public void confirmUserRegistration(String oneTimeCode) {
        var code = codeService.useOneTimeCode(oneTimeCode);
        if (!code.getType().equals(CodeType.VERIFICATION)) {
            log.error("attempting to use code of type: {} for registration.", code.getType());
            throw new CodeException("Invalid code type");
        }

        var user = userRepository.findUserByIotId(code.getUserId())
                .orElseThrow(() -> new UserException("Could not find user."));


        if(user.isVerified()) {
            throw new UserException("User already verified, login instead");
        }

        user.setVerified(true);
        userRepository.save(user);

        emailService.sendEmail(EmailTemplate.confirmationEmail(serverAddress, user.getEmail()));
    }

    @Override
    public void requestPasswordChangeCode(String email) {
        var user = getUserForObject(email);

        if (!user.isVerified()) {
            log.error("User account is not verified yet.");
            throw new UserException("User not verified yet");
        }

        var generatedCode = generateCode(user, CodeType.PASSWORD);
        var confirmationLink = serverAddress+"/reset/"+ generatedCode.getOneTimeCode();

        emailService.sendEmail(EmailTemplate.registrationEmail(confirmationLink, user.getEmail()));
    }

    @Override
    public void resetPassword(String oneTimeCode, String encryptedPassword) {
        var code = codeService.useOneTimeCode(oneTimeCode);
        if (!code.getType().equals(CodeType.PASSWORD)) {
            log.error("attempting to use code of type: {} for password reset.", code.getType());
            throw new CodeException("Invalid code type");
        }

        var user = userRepository.findUserByIotId(code.getUserId())
                .orElseThrow(() -> new UserException("Could not find user."));

        var decodedPassword = new String(Base64.getDecoder().decode(encryptedPassword), StandardCharsets.UTF_8);
        if (passwordEncoder.matches(decodedPassword, user.getPassword())) {
            log.error("User is attempting to use same password. Will block request");
            throw new UserException("New password must be different from previous.");
        }

        user.setPassword(passwordEncoder.encode(decodedPassword));
        var timeOfPasswordChange = LocalDateTime.now();
        user.setLastPasswordChange(timeOfPasswordChange);

        userRepository.save(user);
        log.info("{} changed password @{}", user.getEmail(), timeOfPasswordChange);
    }

    @Override
    public void updateUserName(String email, String username) {
        userRepository.findUserByEmail(email)
                .ifPresent(user -> {
                    user.setName(username);
                    userRepository.save(user);
                    log.info("updated device name to {}", username);
                });
    }

    private Code generateCode(User user, CodeType type) {
        var generatedOneTimeCode = KeyGenerators.string().generateKey();

        return codeService.save(Code.builder()
                .oneTimeCode(generatedOneTimeCode)
                .userId(user.getIotId())
                .type(type)
                .build());
    }
}
