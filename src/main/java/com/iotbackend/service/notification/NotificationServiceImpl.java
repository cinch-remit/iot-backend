package com.iotbackend.service.notification;

import com.iotbackend.api.dto.NotificationDTO;
import com.iotbackend.dao.JobInfoRepository;
import com.iotbackend.dao.NotificationRepository;
import com.iotbackend.domain.jobs.JobInfo;
import com.iotbackend.domain.jobs.JobType;
import com.iotbackend.domain.notification.Notification;
import com.iotbackend.exceptions.NotificationException;
import com.iotbackend.service.notificationdispatcher.NotificationDispatcherService;
import com.iotbackend.service.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.stereotype.Service;

import javax.security.auth.login.AccountException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-28
 * Time: 18:57
 */
@Service
@Slf4j
public class NotificationServiceImpl implements NotificationService {
    private final NotificationRepository notificationRepository;
    private final NotificationDispatcherService notificationDispatcher;
    private final UserService userService;
    private final JobInfoRepository jobInfoRepository;

    public NotificationServiceImpl(NotificationRepository notificationRepository,
                                   NotificationDispatcherService notificationDispatcher, UserService userService,
                                   JobInfoRepository jobInfoRepository) {
        this.notificationRepository = notificationRepository;
        this.notificationDispatcher = notificationDispatcher;
        this.userService = userService;
        this.jobInfoRepository = jobInfoRepository;
    }


    @Override
    public void markAsRead(String email, String notificationId) throws AccountException {
        notificationRepository.findByNotificationId(notificationId)
                .ifPresent(notification -> {
                    if (notification.getUser().getEmail().equals(email)) {
                        notification.setRead(true);
                        notificationRepository.save(notification);
                    }
                });
    }

    @Override
    public void markAllAsRead(String email) throws AccountException {
        var user = userService.getUserForObject(email);
        List<Notification> unreadNotifications = notificationRepository.findAllByIotIdAndReadFalse(user.getIotId());
        unreadNotifications
                .forEach(notification -> notification.setRead(true));
        notificationRepository.saveAll(unreadNotifications);
    }

    @Override
    public List<Notification> getNotifications(String email) throws AccountException {
        var user = userService.getUserForObject(email);
        return notificationRepository
                .findAllByIotId(user.getIotId());
    }

    @Override
    public List<NotificationDTO> getNotificationsForDTO(String email) throws AccountException {
        List<Notification> notifications = getNotifications(email);
        Collections.sort(notifications);
        Collections.reverse(notifications);

        return notifications
                .stream()
                .map(NotificationDTO::new)
                .toList();
    }

    public Notification getNotificationByNotificationId(String email, String notificationId) {
        return notificationRepository.findByNotificationId(notificationId)
                .filter(notification -> notification.getUser().getEmail().equals(email))
                .stream()
                .findFirst()
                .orElseThrow(() -> new NotificationException("Notification with iotId: " + notificationId + " not found"));
    }

    public NotificationDTO getNotificationByNotificationIdForDTO(String email, String notificationId) {
        var notification = getNotificationByNotificationId(email, notificationId);
        return new NotificationDTO(notification);
    }

    @Override
    public void saveAndSend(Notification notification) {
        log.info("received notification:{}", notification);
        NotificationDTO toBeDispatched = saveForDTO(notification);
        notificationDispatcher.dispatch(toBeDispatched);
        log.info("sent notification:{}", notification);
    }

    @Override
    public Map<Object, Object> updateNotificationWithoutNotificationId(String ip, String executedBy) {
        List<String> ids = new ArrayList<>();
         var updated = notificationRepository.findNotificationsByNotificationIdIsNull()
                 .stream()
                 .peek(notification -> {
                     var generatedId = KeyGenerators.string().generateKey();
                     notification.setNotificationId(generatedId);
                     ids.add("" +notification.getId());
                     log.info("updated notificationID {} to notification {}", generatedId, notification.getId());
                 })
                 .collect(Collectors.toList());
         notificationRepository.saveAll(updated);

        Map<String, String> jobDetails = new HashMap<>();
        jobDetails.put("time of command", LocalDateTime.now().toString());
        jobDetails.put("command executed by", executedBy);
        jobDetails.put("from ip", ip);
        jobDetails.put("notifications updated", String.join(",", ids));

        var jobInfo = JobInfo.builder()
                .jobId(KeyGenerators.string().generateKey())
                .type(JobType.NOTIFICATION_ID_JOB)
                .jobDetails(jobDetails)
                .build();
        jobInfoRepository.save(jobInfo);

        return new HashMap<>(jobDetails);
    }
    private Notification save(Notification notification) {
        return notificationRepository.save(notification);
    }

    private NotificationDTO saveForDTO(Notification notification) {
        Notification saved = save(notification);
        return new NotificationDTO(saved);
    }
}
