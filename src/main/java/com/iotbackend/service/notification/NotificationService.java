package com.iotbackend.service.notification;

import com.iotbackend.api.dto.NotificationDTO;
import com.iotbackend.domain.notification.Notification;

import javax.security.auth.login.AccountException;
import java.util.List;
import java.util.Map;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-28
 * Time: 18:56
 */
public interface NotificationService {
    Notification getNotificationByNotificationId(String email, String notificationId);
    NotificationDTO getNotificationByNotificationIdForDTO(String email, String notificationId);
    void markAsRead(String email, String notificationId) throws AccountException;
    void markAllAsRead(String email) throws AccountException;
    List<Notification> getNotifications(String email) throws AccountException;
    List<NotificationDTO> getNotificationsForDTO(String email) throws AccountException;
    Map<Object, Object> updateNotificationWithoutNotificationId(String ip, String executedBy);
    void saveAndSend(Notification notification);
}
