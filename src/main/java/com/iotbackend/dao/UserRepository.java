package com.iotbackend.dao;

import com.iotbackend.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 18:42
 */
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findUserByEmail(String email);
    Optional<User> findUserByIotId(String iotId);
}
