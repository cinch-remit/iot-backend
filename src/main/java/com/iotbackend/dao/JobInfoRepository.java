package com.iotbackend.dao;

import com.iotbackend.domain.jobs.JobInfo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Robinson Mgbah
 * Date: 2021-10-09
 * Time: 08:15
 */
public interface JobInfoRepository extends JpaRepository<JobInfo, Long> {
}
