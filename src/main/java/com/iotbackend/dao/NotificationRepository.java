package com.iotbackend.dao;

import com.iotbackend.domain.notification.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-28
 * Time: 18:47
 */
public interface NotificationRepository extends JpaRepository<Notification, Long> {
    List<Notification> findAllByIotId(String iotId);
    Optional<Notification> findByIotId(String iotId);
    Optional<Notification> findByNotificationId(String notificationId);
    List<Notification> findAllByIotIdAndReadFalse(String iotId);
    List<Notification> findNotificationsByNotificationIdIsNull();
}
