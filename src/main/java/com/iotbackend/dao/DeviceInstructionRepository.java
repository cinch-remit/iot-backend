package com.iotbackend.dao;

import com.iotbackend.domain.device.instruction.DeviceInstruction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @author Robinson Mgbah
 * Date: 2021-07-19
 * Time: 20:57
 */
public interface DeviceInstructionRepository extends JpaRepository<DeviceInstruction, Long> {
    Optional<DeviceInstruction> findByInstructionId(String instructionId);
}
