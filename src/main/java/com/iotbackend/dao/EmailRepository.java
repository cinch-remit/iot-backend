package com.iotbackend.dao;

import com.iotbackend.domain.email.Email;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-18
 * Time: 17:16
 */
public interface EmailRepository extends JpaRepository<Email, Long> {
}
