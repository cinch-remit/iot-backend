package com.iotbackend.dao;

import com.iotbackend.domain.device.Device;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 18:42
 */
public interface DeviceRepository extends JpaRepository<Device, Long> {
}
