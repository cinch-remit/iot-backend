package com.iotbackend.dao;

import com.iotbackend.domain.Reading;
import com.iotbackend.domain.device.Device;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 18:43
 */
public interface ReadingRepository extends JpaRepository<Reading, Long> {
    List<Reading> findAllByConnected(boolean connected);
    List<Reading> findAllByConnectedIsTrueAndConnectionId(String connectionId);
    List<Reading> findAllByDeviceAndConnectedTrue(Device device);
}
