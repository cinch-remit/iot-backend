package com.iotbackend.dao;

import com.iotbackend.domain.User;
import com.iotbackend.domain.dashboard.Dashboard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-30
 * Time: 08:24
 */
@Repository
public interface DashboardRepository extends JpaRepository<Dashboard, Long> {
    Optional<Dashboard> findDashboardByNameAndUser(String name, User user);
    List<Dashboard> findAllByUser(User user);
}
