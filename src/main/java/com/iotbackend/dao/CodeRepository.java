package com.iotbackend.dao;

import com.iotbackend.domain.codes.Code;
import com.iotbackend.domain.codes.CodeType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-18
 * Time: 17:58
 */
public interface CodeRepository extends JpaRepository<Code, Long> {
    Optional<Code> findByOneTimeCode(String oneTimeCode);
    Optional<Code> findByUserIdAndUsed(Long userId, boolean used);
    List<Code> findAllByType(CodeType type);
}
