package com.iotbackend.domain.dashboard;

import com.iotbackend.domain.BaseEntity;
import com.iotbackend.domain.User;
import lombok.*;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-30
 * Time: 07:27
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Dashboard extends BaseEntity{

    private String name;
    private String deviceId;
    private String typeOfReading;
    private String metric;
    private String xAxis;
    private String yAxis;

    @ManyToOne(cascade= {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "user_id")
    private User user;

    @Builder
    public Dashboard(Long id, String name, String deviceId, String typeOfReading, String metric, String xAxis, String yAxis) {
        super(id);
        this.name = name;
        this.deviceId = deviceId;
        this.typeOfReading = typeOfReading;
        this.metric = metric;
        this.xAxis = xAxis;
        this.yAxis = yAxis;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Dashboard dashboard)) return false;
        return Objects.equals(getName(), dashboard.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }

    @Override
    public String toString() {
        return "Dashboard{" +
                "name='" + name + '\'' +
                ", deviceId='" + deviceId + '\'' +
                '}';
    }
}
