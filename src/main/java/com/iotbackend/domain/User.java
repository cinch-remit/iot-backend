package com.iotbackend.domain;

import com.iotbackend.domain.dashboard.Dashboard;
import com.iotbackend.domain.device.Device;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 18:07
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Slf4j
public class User extends BaseEntity{

    @Column(unique = true)
    private String email;
    private String password;
    private String roles;
    private LocalDateTime lastLoggedIn;
    private LocalDateTime dateJoined;
    private LocalDateTime lastPasswordChange;
    private boolean verified;

    @Column(unique = true)
    private String iotId;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Device> devices = new ArrayList<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Dashboard> dashboards = new ArrayList<>();

    private String name;

    @Builder
    public User(Long id, String email, String password, String roles, String iotId) {
        super(id);
        this.email = email;
        this.password = password;
        this.roles = roles;
        this.dateJoined = LocalDateTime.now();
        this.iotId = iotId;
        this.verified = false;
    }

    public void addDevice(Device device) {
        if (!devices.contains(device)) {
            log.info("Device {} not found on user list..will add device to user list", device);
            devices.add(device);
        }
    }

    public void addDashboard(Dashboard dashboard) {
        if (!dashboards.contains(dashboard)) {
            log.info("Dashboard {} not found on dashboard list..will add dashboard to user list", dashboard);
            dashboards.add(dashboard);
        }
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                '}';
    }
}
