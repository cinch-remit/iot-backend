package com.iotbackend.domain.jobs;

import com.iotbackend.domain.BaseEntity;
import lombok.*;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Robinson Mgbah
 * Date: 2021-10-09
 * Time: 07:50
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class JobInfo extends BaseEntity {
    @ElementCollection(fetch = FetchType.EAGER)
    @MapKeyColumn(name="name", length = 100)
    @Column(name="value")
    @CollectionTable(name="job_details", joinColumns=@JoinColumn(name="job_detail_id"))
    private Map<String, String> jobDetails = new HashMap<>();

    private String jobId;

    @Enumerated(EnumType.STRING)
    private JobType type;

    @Builder
    public JobInfo(Long id, Map<String, String> jobDetails, String jobId, JobType type) {
        super(id);
        this.jobDetails = jobDetails;
        this.jobId = jobId;
        this.type = type;
    }
}
