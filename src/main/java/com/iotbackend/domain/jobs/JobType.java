package com.iotbackend.domain.jobs;

import lombok.Getter;

/**
 * @author Robinson Mgbah
 * Date: 2021-10-09
 * Time: 07:52
 */
@Getter
public enum JobType {
    NOTIFICATION_ID_JOB("Notification Id Job", "Adds a new notification_id field to an existing notification");

    private final String name;
    private final String description;

    JobType(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
