package com.iotbackend.domain.notification;

import com.iotbackend.domain.BaseEntity;
import com.iotbackend.domain.User;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-28
 * Time: 18:43
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Notification extends BaseEntity implements Comparable<Notification> {
    @ElementCollection(fetch = FetchType.EAGER)
    @MapKeyColumn(name="name", length = 100)
    @Column(name="value")
    @CollectionTable(name="notification_details", joinColumns=@JoinColumn(name="notification_detail_id"))
    private Map<String, String> notificationDetails = new HashMap<>();

    private LocalDateTime timestamp = LocalDateTime.now();

    @Column(columnDefinition = "boolean default false", name = "read_status")
    private boolean read;

    @Enumerated(value = EnumType.STRING)
    private NotificationType type;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    private String iotId;

    @Column(unique = true)
    private String notificationId;

    @Builder
    public Notification(Long id, User user, NotificationType type, Map<String, String> notificationDetails, String notificationId) {
        super(id);
        this.user = user;
        this.type = type;
        this.notificationDetails = notificationDetails;
        this.iotId = user.getIotId();
        this.notificationId = notificationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Notification that)) return false;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public int compareTo(Notification o) {
        if (o == null) {
            return 1;
        }
        return this.timestamp.compareTo(o.timestamp);
    }

    @Override
    public String toString() {
        return "Notification{" +
                ", timestamp=" + timestamp +
                ", type=" + type +
                ", notificationId='" + notificationId + '\'' +
                '}';
    }
}
