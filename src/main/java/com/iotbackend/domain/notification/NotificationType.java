package com.iotbackend.domain.notification;

import lombok.Getter;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-28
 * Time: 18:41
 */
@Getter
public enum NotificationType {
    DEVICE_ADDED("Device added"), READING_ADDED("Reading added"), DASHBOARD_ADDED("Dashboard added");

    private final String name;

    NotificationType(String name) {
        this.name = name;
    }
}
