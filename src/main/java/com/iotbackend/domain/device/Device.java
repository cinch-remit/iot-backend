package com.iotbackend.domain.device;

import com.iotbackend.domain.BaseEntity;
import com.iotbackend.domain.Reading;
import com.iotbackend.domain.User;
import com.iotbackend.domain.device.instruction.DeviceInstruction;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 18:16
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
public class Device extends BaseEntity {
    private String deviceId;
    private LocalDateTime lastTransmission;

    @OneToMany(mappedBy = "device", cascade = CascadeType.ALL)
    private List<Reading> readings = new ArrayList<>();

    @OneToMany(mappedBy = "device", cascade = CascadeType.ALL)
    private List<DeviceInstruction> instructions = new ArrayList<>();

    @ManyToOne(cascade= {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "user_id")
    private User user;

    private String deviceName;

    @Builder
    public Device(Long id, String deviceId, LocalDateTime lastTransmission) {
        super(id);
        this.deviceId = deviceId;
        this.lastTransmission = lastTransmission;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Device device)) return false;
        return Objects.equals(getDeviceId(), device.getDeviceId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDeviceId());
    }

    @Override
    public String toString() {
        return "Device{" +
                "deviceId='" + deviceId + '\'' +
                '}';
    }
}
