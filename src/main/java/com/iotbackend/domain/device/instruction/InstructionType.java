package com.iotbackend.domain.device.instruction;

import lombok.Getter;

/**
 * @author Robinson Mgbah
 * Date: 2021-07-19
 * Time: 20:50
 */
@Getter
public enum InstructionType {
    MAIL("Mail"),
    TEXT("Text");

    private final String name;

    InstructionType(String name) {
        this.name = name;
    }
}
