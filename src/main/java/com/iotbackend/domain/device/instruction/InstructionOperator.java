package com.iotbackend.domain.device.instruction;

import com.iotbackend.exceptions.DeviceInstructionException;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

/**
 * @author Robinson Mgbah
 * Date: 2021-07-19
 * Time: 20:46
 */
@Getter
public enum InstructionOperator {
    EQUAL("="),
    LESS_THAN("<"),
    LESS_THAN_OR_EQUAL("<="),
    GREATER_THAN(">"),
    GREATER_THAN_OR_EQUAL(">=");

    private final String name;

    InstructionOperator(String name) {
        this.name = name;
    }

    public static InstructionOperator of(String name) {
        return Arrays.stream(InstructionOperator.values())
                .filter(instructionOperator -> StringUtils.equals(instructionOperator.getName(), name))
                .findFirst()
                .orElseThrow(() -> new DeviceInstructionException("Invalid operator"));
    }
}
