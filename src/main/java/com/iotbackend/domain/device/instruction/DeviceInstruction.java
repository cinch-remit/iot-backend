package com.iotbackend.domain.device.instruction;

import com.iotbackend.domain.BaseEntity;
import com.iotbackend.domain.device.Device;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * @author Robinson Mgbah
 * Date: 2021-07-19
 * Time: 20:45
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
public class DeviceInstruction extends BaseEntity {
    private String instructionId;

    private String triggerValue;

    @Enumerated(EnumType.STRING)
    private InstructionOperator instructionOperator;

    @Enumerated(EnumType.STRING)
    private InstructionType instructionType;

    @Enumerated(EnumType.STRING)
    private InstructionStatus instructionStatus;

    private LocalDateTime expireAt;
    private LocalDateTime startAt;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "device_id")
    private Device device;

    @Lob
    private String message;

    @Builder
    public DeviceInstruction(Long id, String instructionId, String triggerValue, InstructionOperator instructionOperator,
                             InstructionType instructionType,
                             LocalDateTime expireAt, LocalDateTime startAt) {
        super(id);
        this.instructionId = instructionId;
        this.triggerValue = triggerValue;
        this.instructionOperator = instructionOperator;
        this.instructionType = instructionType;
        this.instructionStatus = InstructionStatus.INACTIVE;
        this.expireAt = expireAt;
        this.startAt = startAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DeviceInstruction that)) return false;
        return Objects.equals(getTriggerValue(), that.getTriggerValue())
                && getInstructionOperator() == that.getInstructionOperator()
                && getInstructionType() == that.getInstructionType()
                && getInstructionStatus() == that.getInstructionStatus()
                && Objects.equals(getExpireAt(), that.getExpireAt())
                && Objects.equals(getStartAt(), that.getStartAt())
                && Objects.equals(getDevice(), that.getDevice());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTriggerValue(), getInstructionOperator(), getInstructionType(), getInstructionStatus(),
                        getExpireAt(), getStartAt(), getDevice());
    }

    @Override
    public String toString() {
        return "DeviceInstruction{" +
                "instructionId='" + instructionId + '\'' +
                ", triggerValue='" + triggerValue + '\'' +
                ", instructionOperator=" + instructionOperator.getName() +
                ", instructionType=" + instructionType.getName() +
                ", instructionStatus=" + instructionStatus.getName() +
                ", expireAt=" + expireAt +
                ", startAt=" + startAt +
                '}';
    }
}
