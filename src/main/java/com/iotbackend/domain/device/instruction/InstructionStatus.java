package com.iotbackend.domain.device.instruction;

import lombok.Getter;

/**
 * @author Robinson Mgbah
 * Date: 2021-07-19
 * Time: 20:53
 */
@Getter
public enum InstructionStatus {
    ACTIVE("Active"),
    INACTIVE("Inactive");
    private final String name;

    InstructionStatus(String name) {
        this.name = name;
    }
}
