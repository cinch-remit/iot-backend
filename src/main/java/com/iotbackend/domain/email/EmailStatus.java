package com.iotbackend.domain.email;

import lombok.Getter;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-18
 * Time: 17:09
 */
@Getter
public enum EmailStatus {
    CREATED("Created"), SENT("Sent"), FAILED("Failed"), RETRY("Retry");
    private final String name;

    EmailStatus(String name) {
        this.name = name;
    }
}
