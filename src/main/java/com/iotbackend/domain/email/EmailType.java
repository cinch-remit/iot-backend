package com.iotbackend.domain.email;

import lombok.Getter;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-18
 * Time: 17:11
 */
@Getter
public enum EmailType {
    VERIFICATION("Verification"),
    CONFIRMATION("Confirmation"),
    NOTIFICATION("Notification"),
    DEVICE_INSTRUCTION("Device instruction"),
    PASSWORD_RESET("Password reset");

    private final String name;

    EmailType(String name) {
        this.name = name;
    }
}
