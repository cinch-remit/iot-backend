package com.iotbackend.domain.email;

import com.iotbackend.domain.BaseEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-18
 * Time: 17:06
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
public class Email extends BaseEntity {
    private String recipientEmail;
    private String subject;
    private LocalDateTime dateSent;

    @Column(length = 1024)
    private String mailContent;

    @Enumerated(value = EnumType.STRING)
    private EmailType type;

    private String senderEmail;

    @Enumerated(value = EnumType.STRING)
    private EmailStatus status;
    private boolean scheduled;
    private boolean html;
    private int sendCount;
    private LocalDate scheduledDate;

    @Builder
    public Email(Long id, String recipientEmail, String subject, String mailContent, EmailType type, String senderEmail) {
        super(id);
        this.recipientEmail = recipientEmail;
        this.subject = subject;
        this.mailContent = mailContent;
        this.type = type;
        this.senderEmail = senderEmail;
        this.status = EmailStatus.CREATED;
        this.scheduled = false;
        this.html = false;
        this.sendCount = 0;
    }
}
