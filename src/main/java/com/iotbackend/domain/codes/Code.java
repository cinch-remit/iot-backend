package com.iotbackend.domain.codes;

import com.iotbackend.domain.BaseEntity;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import java.time.LocalDateTime;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-18
 * Time: 17:56
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Code extends BaseEntity {
    @CreatedDate
    private LocalDateTime created;

    @LastModifiedDate
    private LocalDateTime usedAt;

    @Column(columnDefinition = "boolean default false")
    private boolean used;


    private String userId;

    @Column(unique = true)
    private String oneTimeCode;

    @Column(columnDefinition = "boolean default false")
    private boolean expired;

    @Enumerated
    private CodeType type;

    @Builder
    public Code(Long id,String userId, String oneTimeCode, CodeType type) {
        super(id);
        this.created = LocalDateTime.now();
        this.used = false;
        this.userId = userId;
        this.oneTimeCode = oneTimeCode;
        this.expired = false;
        this.type = type;
    }
}
