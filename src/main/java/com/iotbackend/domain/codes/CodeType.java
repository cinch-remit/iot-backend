package com.iotbackend.domain.codes;

import lombok.Getter;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-18
 * Time: 17:53
 */
@Getter
public enum CodeType {
    VERIFICATION("Verification"), PASSWORD("Password");

    private final String name;

    CodeType(String name) {
        this.name = name;
    }
}
