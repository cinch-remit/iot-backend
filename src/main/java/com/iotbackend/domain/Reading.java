package com.iotbackend.domain;

import com.iotbackend.domain.device.Device;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 18:26
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
public class Reading extends BaseEntity implements Comparable<Reading>{
    private String value;
    private LocalDateTime timeOfReading;
    private String typeOfReading;
    private String metric;

    private boolean connected;
    private String connectionId;

    @ManyToOne(cascade= {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "device_id")
    private Device device;

    @Builder
    public Reading(Long id, String value, LocalDateTime timeOfReading, String typeOfReading, String metric) {
        super(id);
        this.value = value;
        this.timeOfReading = timeOfReading;
        this.typeOfReading = typeOfReading;
        this.metric = metric;
    }

    @Override
    public int compareTo(Reading o) {
        return o.getTimeOfReading().compareTo(this.getTimeOfReading());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Reading reading)) return false;
        return getTimeOfReading().equals(reading.getTimeOfReading());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTimeOfReading());
    }
}
