package com.iotbackend.configuration.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-30
 * Time: 07:40
 */
@Aspect
@Component
@Slf4j
public class Logging {
    @Around("within(com.iotbackend.controller..*)")
    public Object logEntry(ProceedingJoinPoint joinPoint) throws Throwable {
        var ipAddress = "N/A";
        List<String> userAgent = new ArrayList<>();
        var requestId = "N/A";

        if (joinPoint.getArgs().length > 0) {
            for (Object arg: joinPoint.getArgs()) {
                if (arg instanceof HttpServletRequest s) {
                    ipAddress = s.getRemoteAddr();
                    userAgent = Collections.list(s.getHeaders("User-Agent"));
                    if (SecurityContextHolder.getContext() != null && SecurityContextHolder.getContext().getAuthentication() != null) {
                        requestId = SecurityContextHolder.getContext().getAuthentication().getName();
                    }
                }
            }
        }
        log.info("incoming request:'{}' -->> {}() from ipAddress/user-agent: '{}'/'{}' @ '{}'",
                requestId,
                joinPoint.getSignature().getName(),
                ipAddress,userAgent,
                LocalDateTime.now());

        return joinPoint.proceed();
    }
}
