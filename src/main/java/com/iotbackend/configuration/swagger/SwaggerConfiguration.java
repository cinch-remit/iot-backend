package com.iotbackend.configuration.swagger;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-18
 * Time: 13:29
 */
@Configuration
public class SwaggerConfiguration {

    @Value("${spring.profiles.active}")
    private String env;

    @Value("${swagger.server-address}")
    private String serverAddress;

    @Value("${swagger.contact-name}")
    private String contactName;

    @Value("${swagger.contact-email}")
    private String contactEmail;

    @Value("${git.build.version}")
    private String buildVersion;

    @Bean
    public OpenAPI apiConfiguration() {
        var securityScheme = new SecurityScheme()
                .type(SecurityScheme.Type.HTTP)
                .scheme("basic")
                .name("auth");

        var serverName = new Server()
                .description(env)
                .url(serverAddress);

        var contact = new Contact()
                .name(contactName)
                .email(contactEmail);

        var info = new Info()
                .title("IOT Backend")
                .description("IOT backend for Tillämpad Internet of Things, introduktion(1DT305. Summer 2021) server")
                .version(buildVersion)
                .contact(contact);

        var components = new Components()
                .addSecuritySchemes("auth", securityScheme);

        return new OpenAPI()
                .components(components)
                .info(info)
                .servers(Collections.singletonList(serverName));
    }
}
