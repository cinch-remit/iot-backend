package com.iotbackend.configuration.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.CorsConfigurationSource;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 19:22
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final AuthenticationManager authenticationManager;

    private final CorsConfigurationSource corsConfigurationSource;

    public SecurityConfig(AuthenticationManager authenticationManager, CorsConfigurationSource corsConfigurationSource) {
        this.authenticationManager = authenticationManager;
        this.corsConfigurationSource = corsConfigurationSource;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationManager);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf()
                .disable()
                .cors()
                .configurationSource(corsConfigurationSource)
                .and()
                .authorizeRequests()
                .antMatchers("/**/*.js", "/**/*.css", "/**/*.png", "/favicon/.ico", "/**/*.svg").permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/register", "/forgot","/confirm/**", "/", "/app-info", "/reset/**", "/userops/**", "/resendvercode","/actuator/health").permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/actuator", "/h2-console", "/admin/**").hasAnyAuthority("ADMIN")
                .and()
                .authorizeRequests()
                .antMatchers("/swagger-ui/**", "/swagger-ui.html", "/v3/api-docs/**")
                .hasAnyAuthority("API")
                .and()
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/home", true)
                .permitAll()
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/")
                .and()
                .httpBasic();
    }
}
