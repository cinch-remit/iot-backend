package com.iotbackend.configuration.security;

import com.iotbackend.dao.UserRepository;
import com.iotbackend.domain.User;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.security.auth.message.AuthException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 19:08
 */
@Slf4j
@Component
public class AuthenticationManager implements AuthenticationProvider {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public AuthenticationManager(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @SneakyThrows
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        return userRepository.findUserByEmail(authentication.getName())
                .map(user -> {
                    if (user.isVerified() && passwordEncoder.matches(authentication.getCredentials().toString(), user.getPassword())) {
                        user.setLastLoggedIn(LocalDateTime.now());

                        try {
                            userRepository.save(user);
                        }catch (ObjectOptimisticLockingFailureException e) {
                            userRepository.findById(user.getId())
                                    .ifPresent(u -> {
                                        log.info("Optimistic locking occurred in this transaction..changing object version from {} to {}",user.getVersion(), u.getVersion());
                                        user.setVersion(u.getVersion());
                                    });
                            userRepository.save(user);
                        }

                        log.info("{} logged in @ {}", user, LocalDateTime.now());
                        return new UsernamePasswordAuthenticationToken(user.getEmail(), null, getAuthoritiesFromRoles(user));
                    }
                    return null;
                })
                .orElseThrow(() -> new AuthException("Could not log in user"));
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UsernamePasswordAuthenticationToken.class);
    }

    private List<SimpleGrantedAuthority> getAuthoritiesFromRoles(User user) {
        List<String> roles = Arrays.asList(user.getRoles().split(","));

        return roles.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }
}
