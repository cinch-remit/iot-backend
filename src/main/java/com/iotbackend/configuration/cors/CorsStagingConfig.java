package com.iotbackend.configuration.cors;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

@Configuration
@Profile({"staging"})
public class CorsStagingConfig {

    @Setter
    @Value("${swagger.auth-address}")
    private String authAddress;

    @Setter
    @Value("${swagger.api-address}")
    private String apiAddress;

    @Value("${app.links.frontend}")
    private String frontendLink;

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        var allowedOrigins = List.of(apiAddress, frontendLink);
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(allowedOrigins);
        configuration.addAllowedHeader("*");
        configuration.addAllowedMethod("*");
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
