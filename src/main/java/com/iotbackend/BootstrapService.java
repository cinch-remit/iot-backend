package com.iotbackend;

import com.iotbackend.dao.UserRepository;
import com.iotbackend.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 19:42
 */
@Service
@Slf4j
public class BootstrapService implements ApplicationListener<ContextRefreshedEvent> {
    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    @Value("${bootstrap.email}")
    private String bootstrapEmail;

    @Value("${bootstrap.password}")
    private String bootstrapPassword;

    @Value("${bootstrap.roles}")
    private String bootstrapRoles;

    public BootstrapService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        createBootstrapUser();
    }


    private void createBootstrapUser() {

        if (userRepository.findUserByEmail(bootstrapEmail)
                .isEmpty()) {
            log.info("creating bootstrap user.");
            var user = User.builder()
                    .email(bootstrapEmail)
                    .password(passwordEncoder.encode(bootstrapPassword))
                    .roles(bootstrapRoles)
                    .iotId(KeyGenerators.string().generateKey())
                    .build();

            user.setVerified(true);
            log.info("created user:{} ", userRepository.saveAndFlush(user));
        }
    }
}
