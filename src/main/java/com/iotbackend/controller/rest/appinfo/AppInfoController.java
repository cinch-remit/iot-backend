package com.iotbackend.controller.rest.appinfo;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-23
 * Time: 23:09
 */
@RestController
@RequestMapping("/app-info")
@Slf4j
@Tag(name = "App Information", description = "App Information")
public class AppInfoController {

    @Value("${git.build.version}")
    private String appVersion;

    @Value("${git.commit.id.abbrev}")
    private String buildId;

    @Value("${git.branch}")
    private String buildBranch;

    @Value("${app.links.admin}")
    private String adminLink;

    @Value("${app.information.leadDeveloper}")
    private String leadDeveloper;

    @Value("${app.information.developedBy}")
    private String developedBy;

    @Value("${app.information.location}")
    private String location;

    @Value("${app.information.contact}")
    private String contact;

    @Operation(
            summary = "Get information about version of app currently running."
    )
    @GetMapping
    public Map<String, String> getAppInformation(HttpServletRequest request) {
        var data = new HashMap<String, String>();

        data.put("version", appVersion);
        data.put("buildId", buildId);
        data.put("build", buildBranch);
        data.put("adminLink", adminLink);

        data.put("leadDeveloper", leadDeveloper);
        data.put("developedBy", developedBy);
        data.put("location", location);
        data.put("contact", contact);

        return data;
    }
}
