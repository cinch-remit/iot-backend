package com.iotbackend.controller.rest.user;

import com.iotbackend.service.user.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-18
 * Time: 14:01
 */

@RestController
@RequestMapping("/userops")
@Slf4j
@Tag(name = "User operations", description = "User operations")
public class UserOperationsController {

    private final UserService userService;

    public UserOperationsController(UserService userService) {
        this.userService = userService;
    }

    @Operation(
            summary = "Request a new verification code."
    )
    @PutMapping("/request-verification-code")
    public void requestNewRegistrationCode(@RequestHeader String email, HttpServletRequest request) {
        userService.requestNewRegistrationCode(email);
    }

    @Operation(
            summary = "Request a new password change code."
    )
    @PutMapping("/request-password-code")
    public void requestPasswordResetCode(@RequestHeader String email, HttpServletRequest request) {
        userService.requestPasswordChangeCode(email);
    }

    @Operation(
            summary = "Reset password with one time code."
    )
    @PutMapping("/reset-password/{onetimecode}")
    public void resetPassword(@PathVariable("onetimecode") String oneTimeCode, HttpServletRequest request) {
        userService.confirmUserRegistration(oneTimeCode);
    }
}
