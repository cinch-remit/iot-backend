package com.iotbackend.controller.rest.user;

import com.iotbackend.api.context.UserRegisterContext;
import com.iotbackend.service.user.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-18
 * Time: 14:01
 */

@RestController
@RequestMapping("/userops")
@Slf4j
@Tag(name = "Register user", description = "Register new user")
public class UserRegisterController {

    private final UserService userService;

    public UserRegisterController(UserService userService) {
        this.userService = userService;
    }

    @Operation(
            summary = "Register new user"
    )
    @PostMapping("/register")
    public void registerUser(@RequestBody @Valid UserRegisterContext registerContext, HttpServletRequest request) {
        userService.registerUser(registerContext);
    }

    @PutMapping("/confirm/{onetimecode}")
    public void confirmRegistration(@PathVariable("onetimecode") String oneTimeCode, HttpServletRequest request) {
        userService.confirmUserRegistration(oneTimeCode);
    }
}
