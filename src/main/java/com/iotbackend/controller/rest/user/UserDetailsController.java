package com.iotbackend.controller.rest.user;

import com.iotbackend.api.dto.UserDTO;
import com.iotbackend.service.user.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-18
 * Time: 23:23
 */
@RestController
@RequestMapping("/v1/user")
@Slf4j
@Tag(name = "User Details", description = "User details")
@SecurityRequirement(name = "auth")
public class UserDetailsController {

    private final UserService userService;

    public UserDetailsController(UserService userService) {
        this.userService = userService;
    }

    @Operation(
            summary = "Get a logged in user's details."
    )
    @GetMapping("/details")
    public UserDTO getUserDetails(Authentication authentication, HttpServletRequest request) {
        return userService.getUserForDTO(authentication.getName());
    }

    @Operation(
            summary = "Update a user's name."
    )
    @PatchMapping("/{name}")
    public void updateUsername(Authentication authentication, HttpServletRequest request,@PathVariable("name") String userName) {
        userService.updateUserName(authentication.getName(), userName);
    }
}
