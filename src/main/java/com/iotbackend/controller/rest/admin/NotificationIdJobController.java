package com.iotbackend.controller.rest.admin;

import com.iotbackend.service.notification.NotificationService;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author Robinson Mgbah
 * Date: 2021-10-09
 * Time: 07:42
 */
@RestController
@RequestMapping("/admin/notification")
@Slf4j
@Hidden
@Tag(name = "Admin job Operations", description = "Admin job Operations")
@SecurityRequirement(name = "auth")
public class NotificationIdJobController {
    private final NotificationService notificationService;

    public NotificationIdJobController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @PostMapping("/notification-id-job")
    public Map<Object, Object> triggerNotificationIdJob(HttpServletRequest request, Authentication authentication) {
        var ip = request.getRemoteHost();
        var auth = authentication.getName();

        return notificationService.updateNotificationWithoutNotificationId(ip, auth);
    }
}
