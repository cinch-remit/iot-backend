package com.iotbackend.controller.rest.device;

import com.iotbackend.api.dto.DeviceDTO;
import com.iotbackend.service.device.DeviceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-18
 * Time: 23:32
 */
@RestController
@RequestMapping("/v1/device")
@Slf4j
@Tag(name = "Device Operations", description = "Device Operations")
@SecurityRequirement(name = "auth")
public class DeviceController {

    private final DeviceService deviceService;

    public DeviceController(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @Operation(
            summary = "Register new device."
    )
    @PostMapping("/register/{deviceId}")
    public DeviceDTO registerDevice(Authentication authentication, HttpServletRequest request,@PathVariable("deviceId") String deviceId) {
       return deviceService.registerDevice(authentication.getName(), deviceId);
    }

    @Operation(
            summary = "Get all devices for user."
    )
    @GetMapping("/all")
    public List<DeviceDTO> getAllDevicesForUser(Authentication authentication, HttpServletRequest request) {
        return deviceService.getAllDevicesForUser(authentication.getName());
    }

    @Operation(
            summary = "Get device by id."
    )
    @GetMapping("/{deviceId}")
    public DeviceDTO getDeviceById(Authentication authentication, HttpServletRequest request,@PathVariable("deviceId") String deviceId) {
        return deviceService.getDeviceByIdForDTO(authentication.getName(), deviceId);
    }

    @Operation(
            summary = "Update a device name."
    )
    @PatchMapping("/{deviceId}/{name}")
    public void updateDeviceName(Authentication authentication, HttpServletRequest request,
                                 @PathVariable("deviceId") String deviceId,@PathVariable("name") String deviceName) {
        deviceService.addDeviceName(authentication.getName(), deviceId, deviceName);
    }
}
