package com.iotbackend.controller.rest.device.instruction;

import com.iotbackend.api.context.DeviceInstructionContext;
import com.iotbackend.api.dto.DeviceInstructionDTO;
import com.iotbackend.service.device.instruction.DeviceInstructionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Robinson Mgbah
 * Date: 2021-09-22
 * Time: 05:14
 */
@RestController
@RequestMapping("/v1/device/instruction")
@Slf4j
@Tag(name = "Device Operations", description = "Device Operations")
@SecurityRequirement(name = "auth")
public class DeviceInstructionController {

    private final DeviceInstructionService deviceInstructionService;

    public DeviceInstructionController(DeviceInstructionService deviceInstructionService) {
        this.deviceInstructionService = deviceInstructionService;
    }

    @Operation(
            summary = "Get all device instructions for all devices for user."
    )
    @Tag(name = "Device Instruction operations", description = "Device instruction operations")
    @GetMapping
    public List<DeviceInstructionDTO> getDeviceInstructions(Authentication authentication, HttpServletRequest request) {
        return deviceInstructionService.getAllDeviceInstructions(authentication.getName());
    }

    @Operation(
            summary = "Get instruction for device."
    )
    @Tag(name = "Device Instruction operations", description = "Device instruction operations")
    @GetMapping("/{deviceInstructionId}/{deviceId}")
    public DeviceInstructionDTO getDeviceInstructionForDevice(Authentication authentication, HttpServletRequest request,
                                                              @PathVariable String deviceId,@PathVariable String deviceInstructionId) {
        return deviceInstructionService.getDeviceInstructionById(deviceInstructionId, authentication.getName(), deviceId);
    }

    @Operation(
            summary = "Delete a device instruction."
    )
    @Tag(name = "Device Instruction operations", description = "Device instruction operations")
    @DeleteMapping("/{deviceInstructionId}/{deviceId}")
    public void deleteInstruction(Authentication authentication, HttpServletRequest request,@PathVariable String deviceInstructionId,@PathVariable String deviceId) {
        deviceInstructionService.deleteDeviceInstruction(deviceInstructionId, authentication.getName(), deviceId);
    }

    @Operation(
            summary = "Create a new instruction for device."
    )
    @Tag(name = "Device Instruction operations", description = "Device instruction operations")
    @PostMapping
    public DeviceInstructionDTO createDeviceInstruction(@RequestBody DeviceInstructionContext deviceInstructionContext,
                                                        Authentication authentication, HttpServletRequest request) {
        return deviceInstructionService.createDeviceInstruction(deviceInstructionContext, authentication.getName());
    }
}
