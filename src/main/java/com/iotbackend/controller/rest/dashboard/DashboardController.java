package com.iotbackend.controller.rest.dashboard;

import com.iotbackend.api.context.DashboardContext;
import com.iotbackend.api.dto.DashboardDTO;
import com.iotbackend.service.dashboard.DashboardService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-30
 * Time: 16:43
 */
@RestController
@RequestMapping("/v1/dashboard")
@Slf4j
@Tag(name = "Dashboard Operations", description = "Dashboard Operations")
@SecurityRequirement(name = "auth")
public class DashboardController {

    private final DashboardService dashboardService;

    public DashboardController(DashboardService dashboardService) {
        this.dashboardService = dashboardService;
    }

    @Operation(
            summary = "Create a new dashboard."
    )
    @PostMapping
    public DashboardDTO createDashboard(@Valid @RequestBody DashboardContext dashboardContext, HttpServletRequest request, Authentication authentication) {
        var ownerEmail = authentication.getName();
        return dashboardService.createDashboard(dashboardContext, ownerEmail);
    }

    @Operation(
            summary = "Update an existing dashboard."
    )
    @PutMapping("/{dashboardName}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateDashboard(@RequestBody DashboardContext dashboardContext,
                                @PathVariable("dashboardName") String dashboardName, HttpServletRequest request, Authentication authentication) {
        var ownerEmail = authentication.getName();
        dashboardService.updateDashboard(dashboardContext, dashboardName, ownerEmail);
    }

    @Operation(
            summary = "Delete an existing dashboard."
    )
    @DeleteMapping("/{dashboardName}")
    public void deleteDashboard(@PathVariable("dashboardName") String dashboardName, HttpServletRequest request, Authentication authentication) {
        var ownerEmail = authentication.getName();
        dashboardService.deleteDashboard(dashboardName, ownerEmail);
    }

    @Operation(
            summary = "Get all user dashboards."
    )
    @GetMapping
    public List<DashboardDTO> getAllDashboards(HttpServletRequest request, Authentication authentication) {
        var ownerEmail = authentication.getName();
        return dashboardService.getAllDashboards(ownerEmail);
    }

    @Operation(
            summary = "Get a dashboard by name."
    )
    @GetMapping("/{dashboardName}")
    public DashboardDTO getDashboard(@PathVariable("dashboardName") String dashboardName, HttpServletRequest request, Authentication authentication) {
        var ownerEmail = authentication.getName();
        return dashboardService.getDashboardByName(dashboardName, ownerEmail);
    }
}
