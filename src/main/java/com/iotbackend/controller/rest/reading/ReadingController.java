package com.iotbackend.controller.rest.reading;

import com.iotbackend.api.context.ReadingRegisterContext;
import com.iotbackend.api.dto.ConnectedReadingDTO;
import com.iotbackend.api.dto.ReadingDTO;
import com.iotbackend.service.reading.ReadingService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-18
 * Time: 23:49
 */
@RestController
@RequestMapping("/v1/reading")
@Slf4j
@Tag(name = "Reading Operations", description = "Reading Operations")
@SecurityRequirement(name = "auth")
public class ReadingController {

    private final ReadingService readingService;

    public ReadingController(ReadingService readingService) {
        this.readingService = readingService;
    }

    @Operation(
            summary = "Register a reading for existing device."
    )
    @PutMapping("/register/{deviceId}")
    public void registerReading(HttpServletRequest request, Authentication authentication, @PathVariable("deviceId") String deviceId, @RequestBody ReadingRegisterContext readingRegisterContext) {
        var timeOfReading = LocalDateTime.now();
        readingService.registerReadings(authentication.getName(), deviceId, readingRegisterContext, timeOfReading);
    }

    @Operation(
            summary = "Register connected readings for existing device."
    )
    @PutMapping("/register-connected/{deviceId}")
    public void registerMultipleReadingsAtOnce(HttpServletRequest request, Authentication authentication, @PathVariable("deviceId") String deviceId, @RequestBody List<ReadingRegisterContext> readings) {
        readingService.registerConnectedReadings(authentication.getName(), deviceId, readings);
    }

    @Operation(
            summary = "Get all readings for user."
    )
    @GetMapping("/all")
    public List<ReadingDTO> getAllReadingsForUser(HttpServletRequest request,Authentication authentication) {
        return readingService.getAllReadingsForUser(authentication.getName());
    }

    @Operation(
            summary = "Get all readings for a device."
    )
    @GetMapping("/all/{deviceId}")
    public List<ReadingDTO> getAllReadingsForUserDevice(HttpServletRequest request,Authentication authentication,@PathVariable("deviceId") String deviceId) {
        return readingService.getAllReadingsForDevice(authentication.getName(), deviceId);
    }

    @Operation(
            summary = "Get all readings by type"
    )
    @GetMapping("/type/{type}")
    public List<ReadingDTO> getAllReadingsByType(HttpServletRequest request,Authentication authentication,@PathVariable("type") String type) {
        return readingService.getAllReadingsByType(authentication.getName(), type);
    }

    @Operation(
            summary = "Get a reading by id."
    )
    @GetMapping("/{deviceId}/{readingId}")
    public ReadingDTO getReadingById(HttpServletRequest request,Authentication authentication, @PathVariable("readingId") Long readingId, @PathVariable("deviceId") String deviceId) {
        return readingService.getReadingById(authentication.getName(), deviceId,readingId);
    }

    @Operation(
            summary = "Get all connected readings"
    )
    @GetMapping("/connected/{deviceId}")
    public Map<String, ConnectedReadingDTO> getAllConnectedReadings(HttpServletRequest request, Authentication authentication, @PathVariable("deviceId") String deviceId) {
        return readingService.getConnectedReadings(authentication.getName(), deviceId);
    }
}
