package com.iotbackend.controller.rest.notification;

import com.iotbackend.api.dto.NotificationDTO;
import com.iotbackend.service.notification.NotificationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.security.auth.login.AccountException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-28
 * Time: 19:42
 */

@RestController
@RequestMapping("/v1/notifications")
@Slf4j
@Tag(name = "User Details", description = "User Details")
@SecurityRequirement(name = "auth")
public class NotificationController {

    private final NotificationService notificationService;

    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @Operation(
            summary = "Get all user notifications."
    )
    @Tag(name = "Notifications", description = "Notifications")
    @GetMapping
    public List<NotificationDTO> getUserNotifications(Authentication authentication, HttpServletRequest request) throws AccountException {
        var email = authentication.getName();
        return notificationService.getNotificationsForDTO(email);
    }

    @Operation(
            summary = "Get user notification by id."
    )
    @Tag(name = "Notifications", description = "Notifications")
    @GetMapping("/{id}")
    public NotificationDTO getUserNotificationById(@PathVariable("id") String notificationId,Authentication authentication, HttpServletRequest request)  {
        var email = authentication.getName();
        return notificationService.getNotificationByNotificationIdForDTO(email, notificationId);
    }

    @Operation(
            summary = "Mark a notification as read."
    )
    @Tag(name = "Notifications", description = "Notifications")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("/read/{id}")
    public void readNotification(@PathVariable("id") String iotId, Authentication authentication, HttpServletRequest request) throws AccountException {
        var email = authentication.getName();
        notificationService.markAsRead(email, iotId);
    }

    @Operation(
            summary = "Mark all notifications as read."
    )
    @Tag(name = "Notifications", description = "Notifications")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("/read")
    public void markAllAsRead(Authentication authentication, HttpServletRequest request) throws AccountException {
        var email = authentication.getName();
        notificationService.markAllAsRead(email);
    }
}
