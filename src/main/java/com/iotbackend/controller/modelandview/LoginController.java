package com.iotbackend.controller.modelandview;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-22
 * Time: 09:37
 */
@Controller
@RequestMapping("/")
public class LoginController {

    @Value("${swagger.server-address}")
    private String serverAddress;

    @Value("${git.build.version}")
    private String buildVersion;

    @Value("${swagger.contact-email}")
    private String contactEmail;

    @RequestMapping("/login")
    public ModelAndView authenticate(Model model) {
        var modelAndView = new ModelAndView();
        modelAndView.setViewName("login");

        model.addAttribute("signupLink", serverAddress+"/register");
        model.addAttribute("forgotLink", serverAddress+"/forgot");
        model.addAttribute("contactEmail", contactEmail);
        model.addAttribute("buildVersion", buildVersion);

        return modelAndView;
    }

    @RequestMapping("/logout")
    public String logout() {
        return "logout";
    }
}
