package com.iotbackend.controller;

import com.iotbackend.service.notificationdispatcher.NotificationDispatcherService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;

import javax.security.auth.login.AccountException;
import java.util.List;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-28
 * Time: 19:32
 */
@Slf4j
@Controller
public class NotificationWebSocketController {
    private final NotificationDispatcherService notificationDispatcherService;

    public NotificationWebSocketController(NotificationDispatcherService notificationDispatcherService) {
        this.notificationDispatcherService = notificationDispatcherService;
    }

    @MessageMapping({"/start"})
    public void start(StompHeaderAccessor stompHeaderAccessor) throws AccountException {
        var auth = (UsernamePasswordAuthenticationToken)stompHeaderAccessor.getHeader("simpUser");
        notificationDispatcherService.add(stompHeaderAccessor.getSessionId(), auth.getName());
    }

    @MessageMapping("/stop")
    public void stop(StompHeaderAccessor stompHeaderAccessor){
        List<String> iotId = stompHeaderAccessor.getNativeHeader("id");
        if (iotId != null && !iotId.isEmpty()) {
            var toBeUnsubscribed = iotId.get(0);

            log.info("unsubscribing for {}", toBeUnsubscribed);
            notificationDispatcherService.remove(stompHeaderAccessor.getSessionId(), toBeUnsubscribed);
        }
    }
}
