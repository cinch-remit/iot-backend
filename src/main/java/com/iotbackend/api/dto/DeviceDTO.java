package com.iotbackend.api.dto;

import com.iotbackend.domain.device.Device;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 20:04
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "Device")
public class DeviceDTO {
    private String deviceID;
    private String lastTransmission;
    private List<ReadingDTO> readings;
    private List<DeviceInstructionDTO> deviceInstructions;

    @Builder
    public DeviceDTO(Device device) {
        this.deviceID = device.getDeviceId();
        this.lastTransmission = device.getLastTransmission() == null ? "No Transmission yet" : device.getLastTransmission().toString();
        this.readings = device.getReadings()
                .stream()
                .map(ReadingDTO::new)
                .collect(Collectors.toList());

        this.deviceInstructions = device.getInstructions()
                .stream()
                .map(DeviceInstructionDTO::new)
                .collect(Collectors.toList());
    }
}
