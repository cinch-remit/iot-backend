package com.iotbackend.api.dto;

import com.iotbackend.domain.device.Device;
import lombok.Getter;
import lombok.Setter;

import java.util.Collections;
import java.util.Map;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-24
 * Time: 20:08
 */
@Getter
@Setter
public class DeviceDataDTO extends DeviceDTO {
    private Map<String, ?> data;
    private int totalReadingsForDevice;

    public DeviceDataDTO(Device device, Map<String, ?> data, int totalReadingsForDevice) {
        super(device);
        super.setReadings(Collections.emptyList());
        this.totalReadingsForDevice = totalReadingsForDevice;
        this.data = data;
    }
}
