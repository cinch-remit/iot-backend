package com.iotbackend.api.dto;

import com.iotbackend.domain.User;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 20:03
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "User Information")
@Slf4j
public class UserDTO {
    private String email;
    private List<String> roles;
    private String lastLoggedIn;
    private String id;
    private String name;

    @Builder
    public UserDTO(User user) {
        this.email = user.getEmail();
        this.id = user.getIotId();
        this.roles = Arrays.asList(user.getRoles().split(","));
        if (Objects.nonNull(user.getLastLoggedIn())) {
            this.lastLoggedIn = user.getLastLoggedIn().toString();
        }else {
            log.info("User accessing api with auth..");
        }

        if (Objects.nonNull(user.getName())) {
            this.name = user.getName();
        }
    }
}
