package com.iotbackend.api.dto;

import com.iotbackend.domain.Reading;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.Objects;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 20:05
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "Reading")
public class ReadingDTO {
    private String value;
    private String timeOfReading;
    private String typeOfReading;
    private Long readingId;
    private String metric;
    private boolean connected;
    private String connectionId;

    @Builder
    public ReadingDTO(Reading reading) {
        this.value = reading.getValue();
        this.timeOfReading = reading.getTimeOfReading().toString();
        this.typeOfReading = reading.getTypeOfReading();
        this.readingId = reading.getId();
        this.metric = reading.getMetric();
        this.connected = reading.isConnected();
        this.connectionId = reading.getConnectionId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReadingDTO that)) return false;
        return getReadingId().equals(that.getReadingId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getReadingId());
    }
}
