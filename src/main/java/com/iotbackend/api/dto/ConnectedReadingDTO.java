package com.iotbackend.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.List;
import java.util.Objects;

/**
 * @author Robinson Mgbah
 * Date: 2021-10-11
 * Time: 11:10
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "Connected Reading")
@Builder
public class ConnectedReadingDTO {
    List<ReadingDTO> readings;
    String connectionId;
    String timeOfReading;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConnectedReadingDTO that)) return false;
        return getConnectionId().equals(that.getConnectionId()) && getTimeOfReading().equals(that.getTimeOfReading());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getConnectionId(), getTimeOfReading());
    }

    public boolean checkIfReadingsContainReading(ReadingDTO reading) {
        return readings.contains(reading);
    }
}
