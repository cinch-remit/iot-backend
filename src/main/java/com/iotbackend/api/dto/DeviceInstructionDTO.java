package com.iotbackend.api.dto;

import com.iotbackend.domain.device.instruction.DeviceInstruction;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * @author Robinson Mgbah
 * Date: 2021-07-19
 * Time: 20:59
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "Device Instruction")
public class DeviceInstructionDTO {
    private String id;
    private String triggerValue;
    private String operator;
    private String type;
    private String status;
    private String startTime;
    private String expireTime;
    private String deviceId;

    @Builder
    public DeviceInstructionDTO(DeviceInstruction instruction) {
        this.id = instruction.getInstructionId();
        this.triggerValue = instruction.getTriggerValue();
        this.operator = instruction.getInstructionOperator().getName();
        this.type = instruction.getInstructionType().getName();
        this.status = instruction.getInstructionStatus().getName();
        this.startTime = instruction.getStartAt().toString();
        this.expireTime = instruction.getExpireAt().toString();
        this.deviceId = instruction.getDevice().getDeviceId();
    }
}
