package com.iotbackend.api.dto;

import com.iotbackend.domain.notification.Notification;
import lombok.*;

import java.util.Map;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-28
 * Time: 18:49
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NotificationDTO {

    private Map<String, String> details;
    private String timestamp;
    private boolean read;
    private String type;
    private String iotId;
    private String id;

    @Builder
    public NotificationDTO(Notification notification) {
        this.type = notification.getType().getName();
        this.details = notification.getNotificationDetails();
        this.timestamp = notification.getTimestamp().toString();
        this.read = notification.isRead();
        this.iotId = notification.getIotId();
        this.id = notification.getNotificationId();
    }
}
