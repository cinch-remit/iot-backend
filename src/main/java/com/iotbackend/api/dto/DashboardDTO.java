package com.iotbackend.api.dto;

import com.iotbackend.domain.dashboard.Dashboard;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-30
 * Time: 08:21
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "Dashboard")
public class DashboardDTO {
    private String name;
    private String deviceId;
    private String typeOfReading;
    private String metric;
    private String xAxis;
    private String yAxis;

    public DashboardDTO(Dashboard dashboard) {
        this.name = dashboard.getName();
        this.deviceId = dashboard.getDeviceId();
        this.typeOfReading = dashboard.getTypeOfReading();
        this.metric = dashboard.getMetric();
        this.yAxis = dashboard.getYAxis();
        this.xAxis = dashboard.getXAxis();
    }
}
