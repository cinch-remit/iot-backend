package com.iotbackend.api.context;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-30
 * Time: 08:23
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DashboardContext {

    @NotNull
    private String name;

    @NotNull
    private String deviceId;

    @NotNull
    private String typeOfReading;

    @NotNull
    private String metric;

    @NotNull
    private String xAxis;

    @NotNull
    private String yAxis;
}
