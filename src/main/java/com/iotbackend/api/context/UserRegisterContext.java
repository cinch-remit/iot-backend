package com.iotbackend.api.context;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 20:39
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Schema(name = "user")
public class UserRegisterContext {
    @Email
    @Schema(description = "Email address", required = true, implementation = String.class)
    private String email;

    @NotNull
    @NotBlank
    @Size(min = 9)
    @Schema(description = "Base64 encoded password", required = true, implementation = String.class)
    private String password;
}
