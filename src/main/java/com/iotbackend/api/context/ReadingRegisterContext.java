package com.iotbackend.api.context;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 20:44
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Schema(name = "Reading Request")
public class ReadingRegisterContext {
    private String value;
    private String typeOfReading;
    private String metric;
}
