package com.iotbackend.api.context;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author Robinson Mgbah
 * Date: 2021-07-19
 * Time: 21:07
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeviceInstructionContext {
    @NotNull
    private String triggerValue;

    @NotNull
    private String operator;

    @NotNull
    private String type;

    @NotNull
    private String status;

    @NotNull
    private String startTime;

    @NotNull
    private String expireTime;

    @NotNull
    private String message;

    private String deviceId;
}
