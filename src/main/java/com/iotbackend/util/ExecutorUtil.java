package com.iotbackend.util;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * @author Robinson Mgbah
 * Date: 2021-10-11
 * Time: 10:25
 */
public class ExecutorUtil {
    private ExecutorUtil() {
        throw new IllegalStateException("Utility class");
    }

    public static Executor buildExecutor(int size, int upperBound) {
        return Executors
                .newFixedThreadPool(Math.min(size, upperBound), r -> {
                    Thread thread = new Thread(r);
                    thread.setDaemon(true);
                    return thread;
                });
    }
}
