package com.iotbackend.util;

import com.iotbackend.domain.email.Email;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

import javax.mail.internet.InternetAddress;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-18
 * Time: 17:25
 */
public class EmailUtil {
    public static MimeMessagePreparator mimeMessagePreparatorAttachment(Email email, String attachmentName, String fileName, String devMail) {
        return mimeMessage -> {
            String mailRecipient = StringUtils.isEmpty(devMail) ? email.getRecipientEmail() : devMail;
            var messageHelper = new MimeMessageHelper(mimeMessage, true);
            messageHelper.setFrom(new InternetAddress(email.getSenderEmail(), "IOT Backend 2021"));
            messageHelper.setTo(mailRecipient);
            messageHelper.setSubject(email.getSubject());
            messageHelper.setText(email.getMailContent(), email.isHtml());
            var file = new FileSystemResource(fileName);
            messageHelper.addAttachment(attachmentName, file);
        };
    }

    public static MimeMessagePreparator mimeMessagePreparator(Email email, String devMail, String mailFrom) {
        return mimeMessage -> {
            String mailRecipient = StringUtils.isEmpty(devMail) ? email.getRecipientEmail() : devMail;
            var messageHelper = new MimeMessageHelper(mimeMessage, true);
            messageHelper.setFrom(new InternetAddress(mailFrom, "IOT Backend 2021"));
            messageHelper.setTo(mailRecipient);
            messageHelper.setSubject(email.getSubject());
            messageHelper.setText(email.getMailContent(), email.isHtml());
        };
    }
}
