package com.iotbackend.exceptions;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-28
 * Time: 18:59
 */
public class NotificationException  extends RuntimeException{
    public NotificationException(String message) {
        super(message);
    }
}
