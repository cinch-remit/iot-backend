package com.iotbackend.exceptions;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 21:09
 */
public class DeviceException extends RuntimeException{
    public DeviceException(String message) {
        super(message);
    }
}
