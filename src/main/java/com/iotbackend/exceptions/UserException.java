package com.iotbackend.exceptions;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 20:51
 */
public class UserException extends RuntimeException{
    public UserException(String message) {
        super(message);
    }
}
