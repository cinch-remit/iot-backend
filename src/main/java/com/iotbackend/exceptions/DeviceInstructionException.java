package com.iotbackend.exceptions;

/**
 * @author Robinson Mgbah
 * Date: 2021-07-19
 * Time: 21:17
 */
public class DeviceInstructionException extends RuntimeException{
    public DeviceInstructionException(String message) {
        super(message);
    }
}
