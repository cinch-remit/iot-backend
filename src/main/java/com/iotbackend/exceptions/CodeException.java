package com.iotbackend.exceptions;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-18
 * Time: 18:07
 */
public class CodeException extends RuntimeException{
    public CodeException(String message) {
        super(message);
    }
}
