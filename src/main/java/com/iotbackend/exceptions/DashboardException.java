package com.iotbackend.exceptions;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-30
 * Time: 16:15
 */
public class DashboardException extends RuntimeException{
    public DashboardException(String message) {
        super(message);
    }
}
