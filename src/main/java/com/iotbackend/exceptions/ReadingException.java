package com.iotbackend.exceptions;

/**
 * @author Robinson Mgbah
 * Date: 2021-06-17
 * Time: 21:22
 */
public class ReadingException extends RuntimeException{
    public ReadingException(String message) {
        super(message);
    }
}
