# README #

### What is this repository for? ###

* IOT backend for Tillämpad Internet of Things, introduktion(1DT305. Summer 2021)
* current version `0.0.1-SNAPSHOT`

### How do I get set up? ###

* Install java, maven, (docker and kubernetes are optional)
* There are 2 main profiles when developing `mysql` & `default`.
  Mysql is used when running with a `mysql database`
  When running standalone, no need to specify profile and this uses an in-memory database `H2 database`.

### Running The Application
#### Dependencies
* When running mysql ensure that you have a database connection ready.
* You must provide valid properties for the following if emails are to be sent
  *  spring.mail.host
  *  spring.mail.port
  *  spring.mail.username
  *  spring.mail.password
  
* Bootstrap user default email: `rm222qf@student.lnu.se` and password: `test`. If you decide to deploy this somewhere, make sure you change these bootstrap credentials.
#### Backend(`Springboot application`)
You can run the application from your IDE or alternatively from the terminal using `mvn spring-boot:run`

#### Front End
The front-end is run in react and therefore you need a both `npm` and `node`.
To make and see changes directly in the front-end, you need to do the following

* Start the springboot application
* `cd front-end` which takes you to the front-end application folder
* run `npm run start` which spawns a local server on port `3000`.
  This gives you access to the frontend application and a hot reload server which allows you to see your changes on the fly.

### Database Configuration
There 2 databases,
* H2 in-memory database for `default` profile
* MySQL for `mysql-profile` profile. Default values are
  - username: `iot`
  - password: `iot`
  - url: `jdbc:mysql://localhost:3306/iotbackend?autoReconnect=true&useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true&useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC&transformedBitIsBoolean=true`
If you deploy this application somewhere, be sure to change these database credentials.

#### Kubernetes
There is a standalone [manifest file](standalone-mysql-workload.yaml) for deploying mysql if you dont want to go through the hassle of installing
sql locally. The default values mentioned [here](#database-configuration) apply

### How to run tests
Use maven to run tests (`mvn test`)

### Who do I talk to? ###

* Repo owner or admin
    * Robinson Mgbah - rm222qf@student.lnu.se
    