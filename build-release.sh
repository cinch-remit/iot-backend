#!/bin/bash

week=$(date +"%V")
year=$(date +'%Y')
TAG=$(git describe --tags $(git rev-list --tags --max-count=1))

echo "$year-$week-v$TAG"

jarfile="/target/iot-backend.jar"
app_name="cinchremit/iot-backend"

echo "building release image: $app_name manually using jarfile: $jarfile && version: $TAG"

docker image build --build-arg JAR_FILE=$jarfile -t $app_name:$TAG -t $app_name:latest .
docker image push $app_name:$TAG
docker image push $app_name:latest